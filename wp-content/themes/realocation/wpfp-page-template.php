<?php
    $wpfp_before = "";
    echo "<div class='wpfp-span favorited_homes'>";
    if (!empty($user)) {
        if (wpfp_is_user_favlist_public($user)) {
            $wpfp_before = "$user's Favorite Posts.";
        } else {
            $wpfp_before = "$user's list is not public.";
        }
    }

    if ($wpfp_before):
        echo '<div class="wpfp-page-before">'.$wpfp_before.'</div>';
    endif;

    if ($favorite_post_ids) {
		$favorite_post_ids = array_reverse($favorite_post_ids);
        $post_per_page = -1;
        $page = intval(get_query_var('paged'));

        $qry = array('post__in' => $favorite_post_ids, 'posts_per_page'=> $post_per_page, 'orderby' => 'post__in', 'paged' => $page);
        // custom post type support can easily be added with a line of code like below.
        $qry['post_type'] = array('property');
        query_posts($qry);
        
        echo "<ul class='favorited_homes_ul'>";
        while ( have_posts() ) : the_post();
            echo "<li>";
            echo "<div class='inner'>";
            echo "<div class='image'>";
            echo "<a href='".get_permalink()."' title='".  get_the_title() ."'>";
            if( aviators_get_featured_image(get_the_ID()) ){
              echo '<img src="'.aviators_get_featured_image(get_the_ID(), 320, 190).'" alt="'.get_the_title().'"></a></div>';
            }else{
              echo '<img src="'.get_bloginfo('template_url').'/assets/img/placeholder_bently.jpg" alt="'.get_the_title().'"></a></div>';   
            }
            echo "<div class='title'>";
            echo "<a href='".get_permalink(get_the_ID())."' title='". get_the_title() ."'>" . get_the_title() . "</a>";
            echo "</div></div>";
            wpfp_remove_favorite_link(get_the_ID());
            echo "</li>";
        endwhile;
        echo "</ul>";


        wp_reset_query();
        //echo '<span class="clear_list">'.wpfp_clear_list_link().'</span>';
    } else {
        $wpfp_options = wpfp_get_options();
        echo "<ul class='empty_list'><li>";
        echo $wpfp_options['favorites_empty'];
        echo "</li></ul>";
    }
    echo "</div>";
    wpfp_cookie_warning();
