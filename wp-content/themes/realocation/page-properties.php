<?php
/**
 * Template Name: Properties
 */
?>
<?php
the_post();
if (get_query_var('page')) {
    $paged = get_query_var('page');
}
$id=get_the_ID();
    $val=0;
    if ($id==2249){
        $val='properties_horizontal_search';
    }
    elseif($id==2259){
        $val='packages_horizontal_search';
    }
    elseif($id==2261){
        $val='complete_horizontal_search';
    }
$properties_ho = hydra_form_filter($val);
if ($properties_ho->getFormRecord()) {
    $query_args = $properties_ho->getQueryArray();
    $query_args['post_type']='property';
    $query_args['paged']=$paged;
    //$query_args['meta_query'][]=array('key' => 'hf_property_type_%_value','compare' => 'IN','value' => '273');
}
$sort = aviators_settings_get('property', get_the_ID(), 'sort');
$display_pager = aviators_settings_get('property', get_the_ID(), 'display_pager');

$display = aviators_settings_get('property', get_the_ID(), 'display_type');
if($id==2259){
    $display = 'rowhl';
}

$isotope_taxonomy = aviators_settings_get('property', get_the_ID(), 'isotope_taxonomy');
if (isset($sort) && $sort) {
     aviators_properties_sort_get_query_args(get_the_ID(), $query_args);
}
aviators_properties_filter_get_query_args(get_the_ID(), $query_args);
if ($query_args['posts_per_page']=='1')
   $query_args['posts_per_page']='100';
$fullwidth = !is_active_sidebar('sidebar-1');
switch ($display) {
    case "row":
        $class = empty($fullwidth) ? "col-sm-12" : "col-sm-offset-1 col-sm-10";
        break;
    case "grid":
        $class = empty($fullwidth) ? "col-sm-6 col-md-4" : "col-sm-4 col-md-3";
        break;
    case "isotope":
        $class = empty($fullwidth) ? "col-sm-6 col-md-3" : "col-sm-6 col-md-4";
        break;
    default:
        $class = 'post col-sm-12';
        break;
}

if ($display == 'isotope') {
    $filter_terms = aviators_properties_get_isotope_filter_terms($isotope_taxonomy, get_the_ID());
}

$resolutions = array(
    'xs' => 12,
    'sm' => 6,
    'md' => 4,
    'lg' => 4,
);

if($fullwidth) {
    $resolutions = array(
        'xs' => 12,
        'sm' => 6,
        'md' => 3,
        'lg' => 3,
    );
}
?>

<?php get_header(); ?>
    <div id="main-content" class="col-md-12 col-sm-12 custom_properties_horizontal_search">
        <div class="col-md-3 col-sm-3 custom_filter_form">
            <?php if (is_active_sidebar('content-top')) : dynamic_sidebar('content-top'); endif; ?>
        </div>
        <div class="col-md-9 col-sm-9">
            <h1 <?php if ($display == 'isotope'): ?>class="center"<?php endif; ?>>
                <?php the_title(); ?>
                <?php //aviators_edit_post_link(); ?>
                <?php //aviators_configure_page_link('property', get_the_ID()) ?>
            </h1>
            <?php the_content() ?>

            <?php if (isset($sort) && $sort): ?>
                <?php aviators_get_template('sort', 'property'); ?>
            <?php endif; ?>

            <?php query_posts($query_args); ?>

            <?php if ($display == 'isotope'): ?>
                <?php if ($filter_terms): ?>
                    <ul class="properties-filter">
                        <li class="selected"><a href="#" data-filter="*"><span><?php print __('All', 'aviators'); ?></span></a>
                        </li>
                        <?php foreach ($filter_terms as $slug => $filter_term): ?>
                            <li><a href="#"
                                   data-filter=".property-<?php print $slug; ?>"><span><?php print $filter_term; ?></span></a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>
            <?php endif; ?>
            <div class="properties-items <?php print $display; ?>">
                <div class="items-list">
                    <?php $count = 0; ?>
                    <?php while (have_posts()) : the_post(); ?>
                        <?php
                        $end_line = '';
                        if($display == 'grid') {
                            foreach ($resolutions as $resolution => $columns) {
                                if ($count % (12 / $columns) == 0) {
                                    $end_line .= ' new-line-' . $resolution;
                                }
                            }
                        }

                        ?>

                        <?php if ($display == 'isotope'): ?>
                            <?php $property_class = aviators_properties_append_term_classes($isotope_taxonomy); ?>
                            <div class="property-item <?php print $class; ?> <?php print $property_class; ?>">
                                <?php aviators_get_content_template('property', 'grid'); ?>
                            </div>
                        <?php else: ?>
                            <div class="property-item <?php print $class; ?> <?php print $end_line; ?>">
                                <?php aviators_get_content_template('property', $display); ?>
                            </div>
                        <?php endif; ?>
                        <?php $count++; ?>
                    <?php endwhile; ?>
                </div>
            </div>
            <?php if ($count==0):?>
                <p style="text-align: center;">Sorry, there are no results found. Please try your search again.</p>
            <?php endif ?>
            <!-- /.items-list -->

            <?php if($display_pager): ?>
                <?php aviators_pagination(); ?>
            <?php endif; ?>

            <?php wp_reset_query(); ?>
        </div><!-- /#main-content -->
        <?php if (dynamic_sidebar('content-bottom')) : ?><?php endif; ?>
    </div><!-- /#main-content -->

<?php if ( is_active_sidebar( 'sidebar-1' ) && !aviators_settings_get('property', get_the_ID(), 'disable_sidebar') ): ?>
    <div class="sidebar col-md-3 col-sm-3">
        <?php dynamic_sidebar( 'sidebar-1' ); ?>
    </div><!-- /#sidebar -->
<?php endif; 
if($id==2259){ ?>
 <script type="text/javascript">
    jQuery(document).ready( function(){
        jQuery(".property-row-content .enquire.price_but").click( function(){
            jQuery('.wpcf7-form-control-wrap.menu-807 select').val('H&L packages');
            var text = jQuery(this).parent().siblings(".property-row-data").children("p.address").text();
            jQuery('.wpcf7-form-control-wrap.text-922 input').val(text);
            jQuery("html, body").animate({ scrollTop: jQuery("#pm_widget_text-11").offset().top },'slow');
        });
    });
</script>
 <?php }
 get_footer(); ?>