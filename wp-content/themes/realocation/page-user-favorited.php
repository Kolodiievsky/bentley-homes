<?php
/**
 * Template Name: User Favorited
 */
?>

<?php get_header(); ?>

<?php if ( is_user_logged_in() ) : ?>
 <div id="main-content" class="favorited_homes">
    <div class="col-md-12">
        <?php while ( have_posts() ) : the_post(); ?>
            <?php get_template_part( 'content', 'simple' ); ?>
        <?php endwhile; ?>
        <?php 
          global $wpdb;
            $query = "SELECT post_id, meta_value, post_status FROM $wpdb->postmeta";
            $query .= " LEFT JOIN $wpdb->posts ON post_id=$wpdb->posts.ID";
            $query .= " WHERE post_status='publish' AND meta_key='wpfp_favorites' AND meta_value > 0 ORDER BY ROUND(meta_value) DESC";
            $results = $wpdb->get_results($query);
            if ($results) {
                echo "<ul class='favorited_homes_ul'>";
                foreach ($results as $o):
                    $p = get_post($o->post_id);
                    echo "<li>";
                    echo "<div class='inner'>";
                    echo "<div class='image'>";
                    echo "<a href='".get_permalink($o->post_id)."' title='". $p->post_title ."'>";
                    if( aviators_get_featured_image($o->post_id) ){
                      echo '<img src="'.aviators_get_featured_image($o->post_id, 320, 190).'" alt="'.$p->post_title.'"></a></div>';
                    }else{
                      echo '<img src="'.get_bloginfo('template_url').'/assets/img/placeholder_bently.jpg" alt="'.$p->post_title.'"></a></div>';   
                    }
                    echo "<div class='title'>";
                    echo "<a href='".get_permalink($o->post_id)."' title='". $p->post_title ."'>" . $p->post_title . "</a> ($o->meta_value)</div>";
                    echo "</div></li>";
                endforeach;
                echo "</ul>";
            }
            else{
                echo "<h3>No favorites!</h3>";
            }
        ?>

    </div><!-- /.col-md-4 -->
</div>
<?php endif; ?>

<?php get_footer(); ?>