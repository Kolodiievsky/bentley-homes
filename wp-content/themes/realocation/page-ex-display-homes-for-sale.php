<?php
/**
 * Template Name: Display Homes for Sale
 */
?>
<?php get_header(); ?>
    <div id="main-content" class="col-md-12 col-sm-12 custom_properties_horizontal_search">
        <div class="col-md-3 col-sm-3 custom_filter_form">
           <?php get_sidebar('search_home_designs'); ?>
        </div>
        <div class="col-md-9 col-sm-9">

             <?php while (have_posts()) : the_post(); ?>
                    <h1 class="center"> <?php the_title(); ?></h1>
                     <?php the_content() ?>
             <?php endwhile;  
////////////// search function ////////////////////
$array = array();
$array2 = array();
$array['relation'] = 'AND' ;
if ( isset($_REQUEST["price_filter"]) ) {
    $pstring = $_REQUEST["price_filter"];
    $pr = explode(';', $pstring);
    $array[] = array( 'key' => 'hf_property_price_0_value', 'value'    => $pr , 'type'  => 'NUMERIC' , 'compare' => 'BETWEEN' );
}
else{
    $pstring = "100000;800000";
    $pr = explode(';', $pstring);
    $array[] = array( 'key' => 'hf_property_price_0_value', 'value'    => $pr , 'type'  => 'NUMERIC' , 'compare' => 'BETWEEN' );
}

if ( isset($_REQUEST["land_size"]) ) {
    $land = $_REQUEST["land_size"];
    $pr_land = explode(';', $land);
    $array[] = array( 'key' => 'hf_property_land_size_0_value', 'value'    => $pr_land , 'type'  => 'NUMERIC' , 'compare' => 'BETWEEN' );
}
else{
    $land = "10;18";
    $pr_land = explode(';', $land);
    $array[] = array( 'key' => 'hf_property_land_size_0_value', 'value'    => $pr_land , 'type'  => 'NUMERIC' , 'compare' => 'BETWEEN' );  
}

if ( isset( $_REQUEST['bathrooms_filter']) ) {
    $bathrooms= $_REQUEST['bathrooms_filter'] ;
    $array[] = array( 'key' => 'hf_property_bathrooms_0_value', 'value' =>$bathrooms, 'type'  => 'NUMERIC' , 'compare' => '=' );  
}

if ( isset($_REQUEST['bedrooms_filter']) ) {
    $bedrooms=$_REQUEST['bedrooms_filter'] ;
    $array[] = array( 'key' => 'hf_property_bedrooms_0_value', 'value'  => $bedrooms , 'type'  => 'NUMERIC' , 'compare' => '=' );  
}

if ( isset($_REQUEST['car_spaces_filter']) &&  $_REQUEST['car_spaces_filter'] != 0  ) {
    $car=$_REQUEST['car_spaces_filter'] ;
    $array[] = array( 'key' => 'hf_property_car_spaces_0_value', 'value'  => $car , 'type'  => 'NUMERIC' , 'compare' => '=' );  
}

if ( isset($_REQUEST['storeys_filter']) ) {
    $storeys=$_REQUEST['storeys_filter'] ;
    $array[] = array( 'key' => 'hf_property_storeys_0_value', 'value' =>$storeys, 'type'  => 'NUMERIC' , 'compare' => '=' );  
}

$array2['relation'] = 'AND' ;
if ( isset($_REQUEST['contract-filter']) &&  $_REQUEST['contract-filter'] != 0 ) {
     $st=$_REQUEST['contract-filter'] ;
     $array2[] = array( 'taxonomy' => 'contract_types', 'field'    => 'term_id', 'terms'  => $st );
}

$array2[] = array( 'taxonomy' => 'types', 'field'    => 'term_id', 'terms'  => 283 );
             $args = array(
                'post_type' => 'property',
                'posts_per_page' => -1,
                'meta_query' => $array,
                'tax_query' => $array2,
            );

           //  echo "<pre style='display:none'>";
           // print_r($args);
           // echo "</pre>";
            $the_query = new WP_Query( $args );
            ?>

            <div class="properties-items isotope">
                <div class="items-list">
                    <?php $count = 0; ?>
                    <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>

                     
                            <div class="property-item col-sm-6 col-md-4  isotope-item">
                                <?php aviators_get_content_template('property', 'grid'); ?>
                            </div>
                       
                        <?php $count++; ?>
                    <?php endwhile; ?>
                </div>
            </div>
            <?php if ($count==0):?>
                <p style="text-align: center;">Sorry, there are no results found. Please try your search again.</p>
            <?php endif ?>
            <!-- /.items-list -->

            <?php if($display_pager): ?>
                <?php aviators_pagination(); ?>
            <?php endif; ?>

            <?php wp_reset_query(); ?>
        </div><!-- /#main-content -->
        <?php if (dynamic_sidebar('content-bottom')) : ?><?php endif; ?>
    </div><!-- /#main-content -->

<?php if ( is_active_sidebar( 'sidebar-1' ) && !aviators_settings_get('property', get_the_ID(), 'disable_sidebar') ): ?>
    <div class="sidebar col-md-3 col-sm-3">
        <?php dynamic_sidebar( 'sidebar-1' ); ?>
    </div><!-- /#sidebar -->
<?php endif; 
if($id==2259){ ?>
 <script type="text/javascript">
    jQuery(document).ready( function(){
        jQuery(".property-row-content .enquire.price_but").click( function(){
            jQuery('.wpcf7-form-control-wrap.menu-807 select').val('H&L packages');
            var text = jQuery(this).parent().siblings(".property-row-data").children("p.address").text();
            jQuery('.wpcf7-form-control-wrap.text-922 input').val(text);
            jQuery("html, body").animate({ scrollTop: jQuery("#pm_widget_text-11").offset().top },'slow');
        });
    });
</script>
 <?php }
 get_footer(); ?>