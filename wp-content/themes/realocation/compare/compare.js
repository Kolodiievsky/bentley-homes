(function($) {
    $(document).ready(function() {
        $('#compare-homes .box select.select-wrapper').on('change', function() {
            var parent= $(this).parents('.box').find('.box-content');
            if (this.value!=0){
                $.ajax({
                    cache: false,
                    url: php_array.compare_ajax,
                    data: {ID: this.value},
                    type: 'post',
                    beforeSend: function() {
                        parent.html( '<span class="load">loading...</span>' );
                    },
                    success: function(html) {
                        parent.html(html);
                    }
                });
            }
            else{
                parent.html('');
            }
        });
    });
}(jQuery));

