<?php
include("../../../../wp-load.php");
$id = $_POST['ID'];
$meta = get_post_meta($id);
//print_r($meta);
?>
<script type="text/javascript">
    jQuery(function(){
        jQuery(".post-data a.fancybox").fancybox();
    })
</script>
<div class="post-title">
  <h3><?php echo get_the_title($id); ?></h3>
</div>
<div class="post-image">
    <?php $image_top = wp_get_attachment_image_src( get_post_thumbnail_id( $id ), 'large' );?>
    <?php if(!empty($image_top)):?>
        <div class="image-box">
            <img src="<?php print $image_top[0];?>">
        </div>
    <?php endif ?>
</div>
<div class="col-md-12 post-content">
    <?php print hydra_render_field($id, 'hf_property_short_description', 'detail'); ?>
    <?php $url=get_permalink($id);?>
    <?php if (!empty($url)):?>
        <a class="read_more" target="_blank" href="<?php print $url;?>">More info</a>
    <?php endif ?>
</div>
<div class="col-md-12 post-data">
<ul class="hf_property_overview_custom room">
<?php 
$key_1_value = get_post_meta( $id , 'hf_property_bedrooms', true );
if( !empty($key_1_value)){
    echo '<li class="beds">'.$key_1_value['items'][0]['value'].'</li>';
}
$key_2_value = get_post_meta( $id , 'hf_property_bathrooms', true );
if( !empty($key_2_value)){
    echo '<li class="baths">'.$key_2_value['items'][0]['value'].'</li>';
}
$key_3_value = get_post_meta( $id , 'hf_property_storeys', true );
if( !empty($key_3_value)){
    echo '<li class="storeys">'.$key_3_value['items'][0]['value'].'</li>';
}
$key_4_value = get_post_meta( $id , 'hf_property_car_spaces', true );
if( !empty($key_4_value)){
    echo '<li class="garages">'.$key_4_value['items'][0]['value'].'</li>';
}
$key_5_value = get_post_meta( $id , 'hf_property_area', true );
if( !empty($key_5_value)){
    echo '<li class="area">'.$key_5_value['items'][0]['value'].'</li>';
}
?>
</ul>
<?php
if( get_field('room_dimensions',$id) ){
    echo get_field('room_dimensions',$id);
}
if( get_field('home_dimensions',$id) ){
    echo get_field('home_dimensions',$id);
}
//print_r($key_1_value);
   // print hydra_render_group($id, 'overview', 'detail'); ?>
    <?php if (isset($meta['floor_plan'][0]) && !empty($meta['floor_plan'][0])): ?>
        <div class="overview-item">
            <a class="floor-plan fancybox"
               href="<?php print wp_get_attachment_image_src($meta['floor_plan'][0], 'full')[0]; ?>">
                <?php print wp_get_attachment_image($meta['floor_plan'][0], array(262, 300) ); ?>
            </a>
        </div>
    <?php endif ?>
</div>
