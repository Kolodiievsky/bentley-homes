<?php
/**
 * Template Name: Browse H&L packages
 */
?>
<?php
$paged = get_query_var( 'page', 1 ); 
get_header(); ?>
    <div id="main-content" class="col-md-12 col-sm-12 custom_properties_horizontal_search">
        <div class="col-md-3 col-sm-3 custom_filter_form">
          <?php get_sidebar('packages_horizontal_search'); ?>
        </div>
        <div class="col-md-9 col-sm-9">
           <?php while (have_posts()) : the_post(); ?>
                    <h1> <?php the_title(); ?></h1>
                     <?php the_content() ?>
             <?php endwhile; 

////////////// search function ////////////////////
$array = array();
$array['relation'] = 'AND' ;
if ( isset($_REQUEST["price_filter"]) ) {
    $pstring = $_REQUEST["price_filter"];
    $pr = explode(';', $pstring);
    $array[] = array( 'key' => 'hf_property_price_0_value', 'value'    => $pr , 'type'  => 'NUMERIC' , 'compare' => 'BETWEEN' );
}
else{
    $pstring = "100000;800000";
    $pr = explode(';', $pstring);
    $array[] = array( 'key' => 'hf_property_price_0_value', 'value'    => $pr , 'type'  => 'NUMERIC' , 'compare' => 'BETWEEN' );
}

if ( isset( $_REQUEST['bathrooms_filter']) ) {
    $bathrooms= $_REQUEST['bathrooms_filter'] ;
    $array[] = array( 'key' => 'hf_property_bathrooms_0_value', 'value' => $bathrooms, 'type'  => 'NUMERIC' , 'compare' => '=' );  
}

if ( isset($_REQUEST['bedrooms_filter']) ) {
    $bedrooms=$_REQUEST['bedrooms_filter'] ;
    $array[] = array( 'key' => 'hf_property_bedrooms_0_value', 'value'  => $bedrooms , 'type'  => 'NUMERIC' , 'compare' => '=' );  
}

if ( isset($_REQUEST['car_spaces_filter']) &&  $_REQUEST['car_spaces_filter'] != 0 ) {
    $car=$_REQUEST['car_spaces_filter'] ;
    $array[] = array( 'key' => 'hf_property_car_spaces_0_value', 'value'  => $car , 'type'  => 'NUMERIC' , 'compare' => '=' );  
}

if ( isset($_REQUEST['storeys_filter']) ) {
    $storeys=$_REQUEST['storeys_filter'] ;
    $array[] = array( 'key' => 'hf_property_storeys_0_value', 'value' => $storeys, 'type'  => 'NUMERIC' , 'compare' => '=' );  
}

if ( isset($_REQUEST['suburbs_filter']) &&  $_REQUEST['suburbs_filter'] !== '0' ) {
    $suburbs=$_REQUEST['suburbs_filter'] ;
    $array[] = array( 'key' => 'suburb', 'value' => $suburbs, 'compare' => '=' );       
} 


            $args = array(
                'post_type' => 'property',
                'posts_per_page' => -1,
                'meta_query' => $array,
                'tax_query' => array(
                    array(
                        'taxonomy' => 'types',
                        'field'    => 'term_id',
                        'terms'    => 275 ,
                    ),
                ),
            );

           // echo "<pre>";
           // print_r($args);
           // echo "</pre>";
            $the_query = new WP_Query( $args );
            ?>
            <div class="properties-items">
                <div class="items-list">
                    <?php while ( $the_query->have_posts() ) :
                          $the_query->the_post();
                          //$meta = get_post_meta(get_the_ID());  
                          // echo "<pre style='display:none'>";
                          // print_r($meta);
                          // echo "</pre>";
                          $key_2_value = get_post_meta( get_the_ID() , 'hf_property_price', true );
                          ?>
                         <div class="property-item">
                            <?php aviators_get_content_template('property', 'rowhl' ); ?>
                        </div>
                        <?php $count++; ?>
                    <?php endwhile; wp_reset_postdata(); ?>
                </div>
            </div>
            <?php if ($count==0):?>
                <p style="text-align: center;">Sorry, there are no results found. Please try your search again.</p>
            <?php endif ?>
            <!-- /.items-list -->

            <?php if($display_pager): ?>
                <?php aviators_pagination(); ?>
            <?php endif; ?>

            <?php wp_reset_query(); ?>
            
           </div>
        <?php if (dynamic_sidebar('content-bottom')) : ?><?php endif; ?>
    </div><!-- /#main-content -->

<?php if ( is_active_sidebar( 'sidebar-1' ) && !aviators_settings_get('property', get_the_ID(), 'disable_sidebar') ): ?>
    <div class="sidebar col-md-3 col-sm-3">
        <?php dynamic_sidebar( 'sidebar-1' ); ?>
    </div><!-- /#sidebar -->
<?php endif; ?>
<script type="text/javascript">
    jQuery(document).ready( function(){
        jQuery(".property-row-content .enquire.price_but").click( function(){
            jQuery('.wpcf7-form-control-wrap.menu-807 select').val('H&L packages');
            var text = jQuery(this).parent().siblings(".property-row-data").find("span.street").text();
            jQuery('.wpcf7-form-control-wrap.text-922 input').val(text);
            jQuery("html, body").animate({ scrollTop: jQuery("#pm_widget_text-11").offset().top },'slow');
            return false;
        });
    });
</script>
<?php get_footer(); ?>