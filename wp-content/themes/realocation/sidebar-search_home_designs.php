<div id="hydraform-8" class="hydra-form hydra-frontend-form hydra-frontend-packages_horizontal_search packages_horizontal_search">
   <div class="hydra-form-content">
      <form action="" method="get" name="hydraform-packages_horizontal_search" id="hydraform-packages_horizontal_search">
         <div class="form-set radio-logo">
            <div class="field-widget-wrapper field-widget-number-wrapper field-widget-number-wrapper">
               <div class="field-item-wrapper">
                  <div class="field-item form-group field-type-text"> 
                  <h4>Design Collection</h4>
                  </div>
                   <div class="selector-logo">
<?php 
if ( isset($_REQUEST["price_filter"]) ) {
    $pstring = $_REQUEST["price_filter"];
}
else{
    $pstring = "100000%3B800000";
}
if ( isset($_REQUEST["land_size"]) ) {
    $land = '&land_size=';
    $land .= $_REQUEST["land_size"];
}
else{
  $land='';
}
?>
            <input  id="classic_collection" type="radio" name="contract-filter" value="214" <?php if( isset( $_REQUEST['contract-filter'] ) && $_REQUEST['contract-filter'] == "214" ){ echo "checked='checked' "; } ?> />
            <label class="drinkcard-cc classic_collection" for="classic_collection">
            <?php $image = get_field('image_search', 'contract_types_214' );?>
            <a href="<?php bloginfo('url')?>/home-designs/search-home-designs/?contract-filter=214<?php echo $land;?>&price_filter=<?php echo $pstring;?>&submit=Search">
            <img  src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
            </a>
            </label>
           <input id="designer_collection" type="radio" name="contract-filter" value="210" <?php if(isset($_REQUEST['contract-filter']) && $_REQUEST['contract-filter'] == "210") { echo " checked='checked' "; }?> />
           <label class="drinkcard-cc designer_collection" for="designer_collection">
             <?php $image1 = get_field('image_search', 'contract_types_210' );?>
             <a href="<?php bloginfo('url')?>/home-designs/search-home-designs/?contract-filter=210<?php echo $land;?>&price_filter=<?php echo $pstring;?>&submit=Search">
            <img  src="<?php echo $image1['url']; ?>" alt="<?php echo $image1['alt']; ?>" /></label>
            </a>

           <!--<input id="sloping_sites" type="radio" name="contract-filter" value="277"  <?php //if( isset( $_REQUEST['contract-filter'] ) && $_REQUEST['contract-filter'] == "277" ){ echo " checked='checked' "; } ?> />
            <label class="drinkcard-cc sloping_sites" for="sloping_sites">
                <?php // $image14 = get_field('image_search', 'contract_types_277' );?>
               <img src="<?php // echo $image14['url']; ?>" alt="<?php // echo $image14['alt']; ?>" /></label>-->
            </div>
               </div>
            </div>
         </div>
         <div class="form-set">  
            <div class="field-widget-wrapper field-widget-range_slider-wrapper field-widget-number-wrapper">
               <div class="field-item-wrapper">
                  <div class="field-item form-group field-type-text">
                    <label for="land-size">Lot Width</label>
                    <?php 
                       if( isset( $_REQUEST['land_size'] ) ){ 
                          $land_filter = $_REQUEST['land_size'];
                          $land = explode(";", $land_filter ); 
                          $min = $land[0];
                          $max = $land[1]; 
                       }  
                       else{
                          $min = "10";
                          $max = "18"; 
                          $land_filter = "10;18";
                       } 
                    ?>
                    <input class="form-control hidden ion-range-slider" data-min="10" data-max="18" data-from="<?php echo $min;?>" data-to="<?php echo $max;?>" data-prefix=" " data-step="1" data-postfix="M" data-hasGrid="false" id="land-size" type="text" value="<?php echo $land_filter;?>" name="land_size">
                  </div>
               </div>
            </div>
         </div>

         <div class="form-set">
            <div class="field-widget-wrapper field-widget-range_slider-wrapper field-widget-number-wrapper">
               <div class="field-item-wrapper">
                  <div class="field-item form-group field-type-text">
                    <label for="price-filter">Price from</label>
                    <?php 
                       if( isset( $_REQUEST['price_filter'] ) ){ 
                          $price_filter = $_REQUEST['price_filter'];
                          $price = explode(";", $_REQUEST['price_filter'] );
                          $min = $price[0];
                          $max = $price[1]; 
                       }
                       else{
                          $min = "100000"; 
                          $max = "800000"; 
                          $price_filter = "100000;800000";
                       } 
                    ?>
                    <input class="form-control hidden ion-range-slider" data-min="100000" data-max="800000" data-from="<?php echo $min;?>" data-to="<?php echo $max;?>" data-prefix="$" data-step="5000" data-postfix="" data-hasGrid="false" id="price-filter" type="text" value="<?php echo $price_filter;?>" name="price_filter">
                  </div>
               </div>
            </div>
         </div>
         <div class="form-set">
            <div class="field-widget-wrapper">
               <div class="field-item-wrapper">
                  <div class="field-item form-group field-type-text"> 
                  <h4>Bedrooms</h4>
                  <input id="bedrooms-filter-1" type="radio" value="1" name="bedrooms_filter" <?php if( isset( $_REQUEST['bedrooms_filter'] ) && $_REQUEST['bedrooms_filter'] == 1 ){ echo " checked='checked'"; } ?> ><label for="bedrooms-filter-1">1</label>
                  <input id="bedrooms-filter-2" type="radio" value="2" name="bedrooms_filter" <?php if( isset( $_REQUEST['bedrooms_filter'] ) && $_REQUEST['bedrooms_filter'] == 2 ){ echo " checked='checked'"; } ?>><label for="bedrooms-filter-2">2</label>
                  <input id="bedrooms-filter-3" type="radio" value="3" name="bedrooms_filter" <?php if( isset( $_REQUEST['bedrooms_filter'] ) && $_REQUEST['bedrooms_filter'] == 3 ){ echo " checked='checked'"; } ?>><label for="bedrooms-filter-3">3</label>
                  <input id="bedrooms-filter-4" type="radio" value="4" name="bedrooms_filter" <?php if( isset( $_REQUEST['bedrooms_filter'] ) && $_REQUEST['bedrooms_filter'] == 4 ){ echo " checked='checked'"; } ?>><label for="bedrooms-filter-4">4</label>
                  <input id="bedrooms-filter-5" type="radio" value="5" name="bedrooms_filter" <?php if( isset( $_REQUEST['bedrooms_filter'] ) && $_REQUEST['bedrooms_filter'] == 5 ){ echo " checked='checked'"; } ?>><label for="bedrooms-filter-5">5</label>
                  </div>
               </div>
            </div>
         </div>
         <div class="form-set">
            <div class="field-widget-wrapper field-widget-number-wrapper">
               <div class="field-item-wrapper">
                  <div class="field-item form-group field-type-text">
                  <h4>Bathrooms</h4>
                  <input id="bathrooms-filter-1" type="radio" value="1" name="bathrooms_filter" <?php if( isset( $_REQUEST['bathrooms_filter'] ) && $_REQUEST['bathrooms_filter'] == 1 ){ echo " checked='checked'"; } ?> ><label for="bathrooms-filter-1">1</label>
                  <input id="bathrooms-filter-2" type="radio" value="2" name="bathrooms_filter" <?php if( isset( $_REQUEST['bathrooms_filter'] ) && $_REQUEST['bathrooms_filter'] == 2 ){ echo " checked='checked'"; } ?> ><label for="bathrooms-filter-2">2</label>
                  <input id="bathrooms-filter-3" type="radio" value="3" name="bathrooms_filter" <?php if( isset( $_REQUEST['bathrooms_filter'] ) && $_REQUEST['bathrooms_filter'] == 3 ){ echo " checked='checked'"; } ?> ><label for="bathrooms-filter-3">3</label>
                  </div>
               </div>
            </div>
         </div>
         <div class="form-set">
            <div class="field-widget-wrapper field-widget-number-wrapper">
               <div class="field-item-wrapper">
                  <div class="field-item form-group field-type-text">
                   <h4>Garages</h4>
                   <input id="car_spaces-filter-1" type="radio" value="1" name="car_spaces_filter" <?php if( isset( $_REQUEST['car_spaces_filter'] ) && $_REQUEST['car_spaces_filter'] == 1 ){ echo " checked='checked'"; } ?> ><label for="car_spaces-filter-1">1</label>
                   <input id="car_spaces-filter-2" type="radio" value="2" name="car_spaces_filter" <?php if( isset( $_REQUEST['car_spaces_filter'] ) && $_REQUEST['car_spaces_filter'] == 2 ){ echo " checked='checked'"; } ?> ><label for="car_spaces-filter-2">2</label>
                   </div>
               </div>
            </div>
         </div>
         <div class="form-set">  
            <div class="field-widget-wrapper field-widget-number-wrapper">
               <div class="field-item-wrapper">
                  <div class="field-item form-group field-type-text">
                   <h4>Storeys</h4>
                   <input id="storeys-filter-1" type="radio" value="1" name="storeys_filter" <?php if( isset( $_REQUEST['storeys_filter'] ) && $_REQUEST['storeys_filter'] == 1 ){ echo " checked='checked'"; } ?> ><label for="storeys-filter-1">Single</label>
                   <input id="storeys-filter-2" type="radio" value="2" name="storeys_filter" <?php if( isset( $_REQUEST['storeys_filter'] ) && $_REQUEST['storeys_filter'] == 2 ){ echo " checked='checked'"; } ?> ><label for="storeys-filter-2">Double</label>
                   <!--<input id="storeys-filter-3" type="radio" value="3" name="storeys_filter" <?php // if( isset( $_REQUEST['storeys_filter'] ) && $_REQUEST['storeys_filter'] == 3 ){ echo " checked='checked'"; } ?> ><label for="storeys-filter-3">Split level</label>-->
                  </div>
               </div>  
            </div>
         </div>
         <div class="field-item form-group field-type-submit  field-type-submit ">
           <input class="btn" id="hydra-submit" type="submit" value="Search" name="submit">
         </div>
      </form>
   </div>
</div>