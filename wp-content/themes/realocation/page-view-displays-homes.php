<?php
/**
 * Template Name: View Display Homes Page
 */
?>

<?php get_header(); ?>


    <div class="alert-no-margin">
        <?php echo aviators_render_messages(); ?>
    </div><!-- /.aler-no-margin -->

    <?php if (is_active_sidebar('top')) : ?>
        <?php if (dynamic_sidebar('top')) : ?><?php endif; ?>
    <?php endif ?>

  <!--<div class="container">
        <div class="row">

            <div id="main-content" class="empty col-md-12 col-sm-12">
                <?php //echo aviators_render_messages(); ?>
                <?php //if (dynamic_sidebar('content-top')) : ?><?php //endif; ?>

                <?php// while (have_posts()) : the_post(); ?>
                    <?php //get_template_part('content', get_post_format()); ?>
                <?php //endwhile; ?>
                <?php// if (dynamic_sidebar('content-bottom')) : ?><?php //endif; ?>
            </div>
            
        </div>
      
  </div>-->

<div class="container product display_home" id="main-content">
 <h2 style="display: block;"><?php the_field('text'); ?></h2>
<?php if( have_rows('boody_text') ): ?>
<div class="row">
         <?php while( have_rows('boody_text') ): the_row(); 
         $image = get_sub_field('image');
         $heading = get_sub_field('heading');
         $address = get_sub_field('address');
         $hours = get_sub_field('display_hours');
         $on_display = get_sub_field('on_display');
         $link_text = get_sub_field('link_text');
         $link = get_sub_field('image_link'); 
         $hours_heading = get_sub_field('hours_heading');
         $address_heading = get_sub_field('address_heading');

         ?>
    <div class="col-sm-6 col-md-4">
      <div class="inner">
        <a href="<?php echo $link; ?>">
          <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
       </a>
      <div class="text">
       <h3><?php echo $heading; ?></h3>
        <div class="hours">
          <div class="head">
             <?php echo $hours_heading; ?>
          </div>
          <div class="contn">
           <?php echo $hours; ?>
          </div>
        </div>
        <div class="address">
          <div class="info">
            <div class="head"><?php echo  $pho_head = get_sub_field('phone_heading'); ?></div>
             <div class="contn"><?php echo  $pho_number = get_sub_field('phone_number'); ?></div>
          </div>
           <div class="info">
             <div class="head"><?php echo $address_heading; ?></div>
             <div class="contn">
               <?php echo $address; ?>
             </div>
          </div>
        </div>
        <div class="on_display"><?php echo $on_display; ?></div>
        <div class="link"><?php echo $link_text; ?> </div>
     </div>
     </div>
    </div>
        <?php endwhile; ?>   
</div>
   <?php endif; ?>
</div>
<?php get_footer(); ?>
