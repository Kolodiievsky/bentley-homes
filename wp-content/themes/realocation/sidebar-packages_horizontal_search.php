<div id="hydraform-8" class="hydra-form hydra-frontend-form hydra-frontend-packages_horizontal_search packages_horizontal_search">
   <div class="hydra-form-content">
      <form action="" method="get" name="hydraform-packages_horizontal_search" id="hydraform-packages_horizontal_search">
         <div class="form-set">
            <div class="field-widget-wrapper field-widget-number-wrapper field-widget-number-wrapper">
               <div class="field-item-wrapper">
                  <div class="field-item form-group field-type-text"> 
                  <h4>Suburbs</h4>
                  <select name="suburbs_filter">
                        <option value="0">Any</option>
                        <?php 
                               $args2 = array(
                                     'post_type' => 'property',
                                     'posts_per_page' => -1,
                                     'meta_query' => array(
                                          array(
                                             'key'     => 'suburb',
                                             'value'   => '',
                                             'compare' => '!=',
                                          ),
                                       ),
                                 );
                     $suburbs = array();
                     $the_query2 = new WP_Query( $args2 );
                      if( $the_query2->have_posts() ): 
                           while( $the_query2->have_posts() ) : $the_query2->the_post(); 
                             $suburbs[] = get_field('suburb'); endwhile; wp_reset_query();
                       endif;    
                          if( !empty($suburbs) ) {
                             $suburbs = array_unique($suburbs);
                             foreach ($suburbs as $value) { ?>
                             <option value="<?php echo $value;?>" <?php if( isset( $_REQUEST['suburbs_filter'] ) && $_REQUEST['suburbs_filter'] == $value ){ echo " selected='selected'"; } ?> ><?php echo $value;?></option>
                           <?php }
                          }
                         
                         ?>   
                  </select>
                  </div>
               </div>
            </div>
         </div>
         <div class="form-set">
            <div class="field-widget-wrapper field-widget-range_slider-wrapper field-widget-number-wrapper">
               <div class="field-item-wrapper">
                  <div class="field-item form-group field-type-text">
                    <label for="price-filter">Price from</label>
                    <?php 
                       if( isset( $_REQUEST['price_filter'] ) ){ 
                          $price_filter = $_REQUEST['price_filter'];
                          $price = explode(";", $_REQUEST['price_filter'] );
                          $min = $price[0];
                          $max = $price[1]; 
                       }
                       else{
                          $min = "100000";
                          $max = "800000"; 
                          $price_filter = "100000;800000";
                       } 
                    ?>
                    <input class="form-control hidden ion-range-slider" data-min="100000" data-max="800000" data-from="<?php echo $min;?>" data-to="<?php echo $max;?>" data-prefix="$" data-step="5000" data-postfix="" data-hasGrid="false" id="price-filter" type="text" value="<?php echo $price_filter;?>" name="price_filter">
                  </div>
               </div>
            </div>
         </div>
         <div class="form-set">
            <div class="field-widget-wrapper">
               <div class="field-item-wrapper">
                  <div class="field-item form-group field-type-text"> 
                  <h4>Bedrooms</h4>
                  <input id="bedrooms-filter-1" type="radio" value="1" name="bedrooms_filter" <?php if( isset( $_REQUEST['bedrooms_filter'] ) && $_REQUEST['bedrooms_filter'] == 1 ){ echo " checked='checked'"; } ?> ><label for="bedrooms-filter-1">1</label>
                  <input id="bedrooms-filter-2" type="radio" value="2" name="bedrooms_filter" <?php if( isset( $_REQUEST['bedrooms_filter'] ) && $_REQUEST['bedrooms_filter'] == 2 ){ echo " checked='checked'"; } ?>><label for="bedrooms-filter-2">2</label>
                  <input id="bedrooms-filter-3" type="radio" value="3" name="bedrooms_filter" <?php if( isset( $_REQUEST['bedrooms_filter'] ) && $_REQUEST['bedrooms_filter'] == 3 ){ echo " checked='checked'"; } ?>><label for="bedrooms-filter-3">3</label>
                  <input id="bedrooms-filter-4" type="radio" value="4" name="bedrooms_filter" <?php if( isset( $_REQUEST['bedrooms_filter'] ) && $_REQUEST['bedrooms_filter'] == 4 ){ echo " checked='checked'"; } ?>><label for="bedrooms-filter-4">4</label>
                  <input id="bedrooms-filter-5" type="radio" value="5" name="bedrooms_filter" <?php if( isset( $_REQUEST['bedrooms_filter'] ) && $_REQUEST['bedrooms_filter'] == 5 ){ echo " checked='checked'"; } ?>><label for="bedrooms-filter-5">5</label>
                  </div>
               </div>
            </div>
         </div>
         <div class="form-set">
            <div class="field-widget-wrapper field-widget-number-wrapper">
               <div class="field-item-wrapper">
                  <div class="field-item form-group field-type-text">
                  <h4>Bathrooms</h4>
                  <input id="bathrooms-filter-1" type="radio" value="1" name="bathrooms_filter" <?php if( isset( $_REQUEST['bathrooms_filter'] ) && $_REQUEST['bathrooms_filter'] == 1 ){ echo " checked='checked'"; } ?> ><label for="bathrooms-filter-1">1</label>
                  <input id="bathrooms-filter-2" type="radio" value="2" name="bathrooms_filter" <?php if( isset( $_REQUEST['bathrooms_filter'] ) && $_REQUEST['bathrooms_filter'] == 2 ){ echo " checked='checked'"; } ?> ><label for="bathrooms-filter-2">2</label>
                  <input id="bathrooms-filter-3" type="radio" value="3" name="bathrooms_filter" <?php if( isset( $_REQUEST['bathrooms_filter'] ) && $_REQUEST['bathrooms_filter'] == 3 ){ echo " checked='checked'"; } ?> ><label for="bathrooms-filter-3">3</label>
                  </div>
               </div>
            </div>
         </div>
         <div class="form-set">
            <div class="field-widget-wrapper field-widget-number-wrapper">
               <div class="field-item-wrapper">
                  <div class="field-item form-group field-type-text">
                   <h4>Garages</h4>
                   <input id="car_spaces-filter-1" type="radio" value="1" name="car_spaces_filter" <?php if( isset( $_REQUEST['car_spaces_filter'] ) && $_REQUEST['car_spaces_filter'] == 1 ){ echo " checked='checked'"; } ?> ><label for="car_spaces-filter-1">1</label>
                   <input id="car_spaces-filter-2" type="radio" value="2" name="car_spaces_filter" <?php if( isset( $_REQUEST['car_spaces_filter'] ) && $_REQUEST['car_spaces_filter'] == 2 ){ echo " checked='checked'"; } ?> ><label for="car_spaces-filter-2">2</label>
                   </div>
               </div>
            </div>
         </div>
         <div class="form-set">
            <div class="field-widget-wrapper field-widget-number-wrapper">
               <div class="field-item-wrapper">
                  <div class="field-item form-group field-type-text">
                   <h4>Storeys</h4>
                   <input id="storeys-filter-1" type="radio" value="1" name="storeys_filter" <?php if( isset( $_REQUEST['storeys_filter'] ) && $_REQUEST['storeys_filter'] == 1 ){ echo " checked='checked'"; } ?> ><label for="storeys-filter-1">Single</label>
                   <input id="storeys-filter-2" type="radio" value="2" name="storeys_filter" <?php if( isset( $_REQUEST['storeys_filter'] ) && $_REQUEST['storeys_filter'] == 2 ){ echo " checked='checked'"; } ?> ><label for="storeys-filter-2">Double</label>

                  </div>
               </div>
            </div>
         </div>
         <div class="field-item form-group field-type-submit  field-type-submit ">
           <input class="btn" id="hydra-submit" type="submit" value="Search" name="submit">
         </div>
      </form>
   </div>
</div>