</div><!-- /.row -->
</div><!-- /.block-content-inner -->
</div><!-- /.block-content-small-padding -->

</div><!-- /.container -->
</div><!-- /#main-inner -->
</div><!-- /#main -->
</div><!-- /#main-wrapper -->

<?php
if(is_front_page()) {
   if( ! get_field('hide_promotions',get_the_ID()) ){
       dynamic_sidebar('bottomslide'); 
     }
}
?>

<div class="container">
    <?php
    if(!is_singular( 'property' ))
        dynamic_sidebar('bottom');
    else
        dynamic_sidebar('bottom_property');
    ?>
</div>
<div id="footer-wrapper">
    <div id="footer">
        <div id="footer-inner">
            <?php if (aviators_active_sidebars(array('footer-left', 'footer-right', 'footer-lower-left', 'footer-lower-right'), AVIATORS_SIDEBARS_ANY)): ?>
                <?php require_once 'templates/footer-top.php'; ?>
            <?php endif; ?>

            <?php if (aviators_active_sidebars(array('footer-bottom'))): ?>

                <?php require_once 'templates/footer-bottom.php'; ?>
            <?php endif; ?>
        </div><!-- /#footer-inner -->
    </div><!-- /#footer -->
</div><!-- /#footer-wrapper -->
</div><!-- /#wrapper -->

<?php wp_footer(); ?>
<?php aviators_footer(); ?>
<script type="text/javascript">
     var map = new google.maps.Map(document.getElementById("map2"));
     //map.setOptions({disableDoubleClickZoom: false });
     map.set('disableDoubleClickZoom', false);
</script>

<!-- Google Code for Remarketing Tag -->
<?php
if ( is_user_logged_in() ) {
    $user = "loggedin";
} else {
    $user = "loggedout";
}
?>
<script type="text/javascript">
jQuery(document).ready( function() {
  var user = "<?php echo $user;?>";
    var ajaxurl="<?php echo admin_url('admin-ajax.php');?>";
    jQuery('.wpfp-link').live('click', function() {
          if( user == "loggedout" ){
            //console.log(user);
           // console.log( Cookies.get()  );
           var title = jQuery(this).attr('title');
            var cookie_compare1 = Cookies.get();
            
                jQuery.ajax({
                  url : ajaxurl,
                  type : 'post',
                  data : {
                    action : 'get_saved_list_cookie',
                    cookie_cache1 : cookie_compare1,
                    title : title
                  },
                  success : function( response ) {
                    console.log(response);
                     jQuery('.header-top span.saved_home a').text(response);
                  }
                 });
            
          }
          else{
            setTimeout(function(){
                jQuery.ajax({
                  url : ajaxurl,
                  type : 'post',
                  data : {
                    action : 'get_saved_list',
                  },
                  success : function( response ) {
                     jQuery('.header-top span.saved_home a').text(response);
                  }
                 });
            }, 200);
          }
            
    });

    jQuery('body').delegate('.save_compare', 'click', function(){
       var no = jQuery(".filters-heading a .compare_no").text();
        console.log(no);
            var home_id = jQuery(this).data('id');
            if ( Cookies.get('compare_list') != undefined ){
                var cookie_compare = Cookies.get('compare_list');
            }
            else{
                var cookie_compare = '';
            }
            jQuery.ajax({
                      url : ajaxurl,
                      type : 'post',
                      data : {
                        action: 'compare_homes',
                        id : home_id,
                        cookie_cache : cookie_compare,
                      },
                      success : function( response ) {
                         console.log(response);
                         jQuery(".filters-heading a .compare_no").text(response);
                      }
            });      
            if( no < 3 ){
              if( jQuery(this).hasClass('active_compare') ){
               jQuery(this).removeClass('active_compare');
              }
              else{
                  jQuery(this).addClass('active_compare');
              } 
            }
            
      return false;
    });


      if ( (jQuery(window).width() > 1023 ) && ( jQuery("#rev_slider_1_1").length > 0 ) ) {
        revapi1.bind("revolution.slide.onloaded",function (e) {
            var height1 = jQuery(window).height();
            var height2 = jQuery("#header-wrapper").outerHeight( true );
            var height4 = jQuery(".box-custom-home").height();
            var height42 = height4/2;
            var result =  height1 - Math.abs(height2) - Math.abs(height42);
            var result2 = result/2;
            jQuery("#rev-slider-widget-2").height(result);
            jQuery("#rev_slider_1_1_wrapper").height(result);
            jQuery("#rev_slider_1_1_wrapper #rev_slider_1_1").height(result);
            jQuery("#rev-slider-widget-2 .tp-fullwidth-forcer").height(result);
            jQuery("#rev_slider_1_1_wrapper #rev_slider_1_1 .tp-bgimg.defaultimg").height(result);
            jQuery(".box-custom-home").css( "margin-top", - Math.abs(height42) );
            jQuery(".tparrows").css( "top", result2 );
           // console.log(result+"&&"+height4);
        });
                
        revapi1.bind("revolution.slide.onchange",function (e,data) {
            var height1 = jQuery(window).height();
            var height2 = jQuery("#header-wrapper").outerHeight( true );
            var height4 = jQuery(".box-custom-home").height();
            var height42 = height4/2;
            var result =  height1 - Math.abs(height2) - Math.abs(height42);
            var result2 = result/2;
            jQuery("#rev-slider-widget-2").height(result);
            jQuery("#rev_slider_1_1_wrapper").height(result);
            jQuery("#rev_slider_1_1_wrapper #rev_slider_1_1").height(result);
            jQuery("#rev-slider-widget-2 .tp-fullwidth-forcer").height(result);
            jQuery("#rev_slider_1_1_wrapper #rev_slider_1_1 .tp-bgimg.defaultimg").height(result);
            jQuery(".box-custom-home").css( "margin-top", - Math.abs(height42) );
            jQuery(".tparrows").css( "top", result2 );
          // console.log(result+"&&"+height4);
        }); 
      }  

    });
    jQuery(function(){
        jQuery(".wpgmp_map_container").append('<div class="map-over"></div>');
        jQuery("#menu-social li a").attr("target", "_blank");
        jQuery(".menu-949s select option:first").val('');
        jQuery(".menu-807s select option:first").val('');
        jQuery( ".form-input-item" ).each(function( index ) {
            jQuery(this).find("input").attr("placeholder", jQuery(this).data('name') );
        });
    });

    /* <![CDATA[ */
    var google_conversion_id = 985744057;
    var google_custom_params = window.google_tag_params;
    var google_remarketing_only = true;
    /* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
    <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/985744057/?value=0&amp;guid=ON&amp;script=0"/>
    </div>
</noscript>
</body>
</html>