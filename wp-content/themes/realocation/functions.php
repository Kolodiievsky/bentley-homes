<?php

define ('AVIATORS_SIDEBARS_ALL', 1);
define ('AVIATORS_SIDEBARS_ANY', 0);

define('THEMENAME', 'realocation');

require_once get_template_directory() . '/launcher/launcher.php';
/*****************************************************************
 * Misc
 *****************************************************************/
require_once get_template_directory() . '/core/class-tgm-plugin-activation.php';
require_once get_template_directory() . '/core/aq_resizer.php';

/*****************************************************************
 * Settings
 *****************************************************************/
require_once get_template_directory() . '/core/settings/tgm.php';
require_once get_template_directory() . '/core/settings/menus.php';
require_once get_template_directory() . '/core/settings/sidebars.php';
require_once get_template_directory() . '/core/settings/scripts.php';
require_once get_template_directory() . '/core/settings/customizations.php';

/*****************************************************************
 * Utility
 *****************************************************************/
require_once get_template_directory() . '/utility/image.php';
require_once get_template_directory() . '/utility/templates.php';
require_once get_template_directory() . '/utility/comments.php';
require_once get_template_directory() . '/utility/pagination.php';
require_once get_template_directory() . '/utility/settings.php';


/**
 * Define steps for launcher
 * @param $steps
 * @return mixed
 */
function realocation_aviators_launcher_steps($steps) {

    $steps['content'] = array(
        'title' => __('Content Import', 'aviators'),
        'importer' => 'content',
        'file' => dirname(__FILE__) . '/exports/realocation_content.xml',
    );

    $steps['hydra'] = array(
        'title' => __('Hydra Import', 'aviators'),
        'importer' => 'hydra',
        'file' => dirname(__FILE__) . '/exports/hydra_export.xml',
    );

    $steps['widget'] = array(
        'title' => __('Widget Import', 'aviators'),
        'importer' => 'widget-settings',
        'file' => dirname(__FILE__) . '/exports/widget_data.json',
    );

    $steps['logic'] = array(
        'title' => __('Widget Logic Import', 'aviators'),
        'importer' => 'widget-logic',
        'file' => dirname(__FILE__) . '/exports/widget_logic.json',
    );

    $steps['theme'] = array(
        'title' => __('Theme Options', 'aviators'),
        'importer' => 'theme-options',
        'file' => dirname(__FILE__) . '/exports/theme_options.json',
    );

    return $steps;
}
add_filter('aviators_launcher_steps', 'realocation_aviators_launcher_steps');

if (!isset($content_width)) {
    $content_width = 1170;
}

function aviators_footer() {
    $instance = NULL;
   // do_action('aviators_footer_map_widget', $instance);
   // do_action('aviators_footer_map_detail');
}

function aviators_entry_meta() {
    if (is_sticky() && is_home() && !is_paged()) {
        echo '<span class="featured-post">' . __('Sticky', 'aviators') . '</span>';
    }

    if (!has_post_format('link') && 'post' == get_post_type()) {
        aviators_entry_date();
    }

    $tag_list = get_the_tag_list('', __(', ', 'aviators'));
    if ($tag_list) {
        echo '<span class="tags-links">' . $tag_list . '</span>';
    }

    if (in_array('category', get_object_taxonomies(get_post_type()))) {
        echo '<div class="entry-meta"><span class="cat-links">' . get_the_category_list(',') . '</span></div>';
    }

    if ('post' == get_post_type()) {
        $author_posts_url = esc_url(get_author_posts_url(get_the_author_meta('ID')));
        $author_title = esc_attr(sprintf(__('View all posts by %s', 'aviators'), get_the_author()));
        $author = get_the_author();
        print '<span class="author vcard">' . __('Posted by', 'aviators') . ' <a class="url fn n" href="' . $author_posts_url . '" title="' . $author_title . '" rel="author">' . $author . '</a></span> ' . __('on', 'aviators') . ' ' . aviators_entry_date();
    }
}

function aviators_link_pages() {
    wp_link_pages(array(
        'before' => '<div class="page-links"><span class="page-links-title">' . __('Pages:', 'aviators') . '</span>',
        'after' => '</div>',
        'link_before' => '<span>',
        'link_after' => '</span>'
    ));
}

function aviators_comments_popup_link() {
    comments_popup_link(
        '<span class="leave-reply">' . __('Leave a comment', 'aviators') . '</span>',
        __('One comment so far', 'aviators'),
        __('View all % comments', 'aviators')
    );
}

function aviators_the_content() {
    the_content(__('Continue reading', 'aviators'));
}

function aviators_morelink_class($link, $text) {
    return str_replace(
        'more-link', 'more-link btn arrow-right btn-primary', $link
    );
}

add_action('the_content_more_link', 'aviators_morelink_class', 10, 2);

function aviators_entry_date($echo = FALSE) {
    $format_prefix = (has_post_format('chat') || has_post_format('status')) ? __('%1$s on %2$s', '1: post format name. 2: date', 'aviators') : '%2$s';

    $date = sprintf('<span class="date"><a href="%1$s" title="%2$s" rel="bookmark"><time class="entry-date" datetime="%3$s">%4$s</time></a></span>',
        esc_url(get_permalink()),
        esc_attr(sprintf(__('Permalink to %s', 'aviators'), the_title_attribute('echo=0'))),
        esc_attr(get_the_date('c')),
        esc_html(sprintf($format_prefix, get_post_format_string(get_post_format()), get_the_date()))
    );

    if ($echo) {
        echo $date;
    }

    return $date;
}

/**
 * Settings page link
 * @param $post_type
 * @param $page_id
 */
function aviators_configure_page_link($post_type, $page_id) {
    // @todo access check
    if (!current_user_can('edit_pages')) {
        return;
    }
    $path = aviators_settings_get_settings_path($post_type, $page_id);

    $output = '<span class="edit-link"><a href=' . $path . '>';
    $output .= '<i class="fa fa-cog"></i>' . __("Configure", 'aviators');
    $output .= '</a></span>';

    print $output;
}

/**
 * Edit post link
 */
function aviators_edit_post_link($post_id = 0) {
    edit_post_link(__('<i class="fa fa-pencil"></i> Edit', 'aviators'), '<span class="edit-link">', '</span>', $post_id);
}

/**
 * Better excerpt read more link
 */
add_filter('excerpt_more', 'aviators_excerpt_more');
function aviators_excerpt_more($more) {
    return '<div class="read-more-wrapper"><a target="_blank" class="btn btn-primary" href="' . get_permalink(get_the_ID()) . '">' . __('Read More', 'aviators') . ' </a></div>';
}

/**
 * Additional theme setup functions
 */
add_action('after_setup_theme', 'aviators_theme_setup');
function aviators_theme_setup() {
    load_theme_textdomain('aviators', get_template_directory() . '/languages');
    add_editor_style();

    add_theme_support('automatic-feed-links');
    add_theme_support('post-thumbnails');
    add_image_size( 'facade-thumb', 350, 150, true );
    add_image_size( 'floor_plan_compare', 317, 350, true );
    add_image_size( 'slider-thumb', 291, 173, true ); // (cropped)
    add_image_size( '3dvirtualtours-thumb', 300, 200, true ); // (cropped)
    // add_theme_support('custom-header');
    // add_theme_support('custom-background');

    add_filter('widget_text', 'do_shortcode');

    if (current_user_can('subscriber')) {
        remove_action('wp_footer', 'wp_admin_bar_render', 1000);
        add_filter('body_class', 'aviators_remove_admin_bar_for_subscriber');
    }
}

function aviators_remove_admin_bar_for_subscriber($classes) {
    $result = array();

    foreach ($classes as $class) {
        if ($class != 'admin-bar') {
            $result[] = $class;
        }
    }

    return $result;
}

/**
 * Helper function for content loop to know if there it is next post in loop
 */
function more_posts() {
    global $wp_query;
    return $wp_query->current_post + 1 < $wp_query->post_count;
}

/**
 * Nice formatted title tag value
 */
add_filter('wp_title', 'aviators_wp_title', 10, 2);
function aviators_wp_title($title, $sep) {
    global $paged, $page;

    if (is_feed()) {
        return $title;
    }


    // Add the site name.
    $title .= get_bloginfo('name');

    // Add the site description for the home/front page.
    $site_description = get_bloginfo('description', 'display');
    if ($site_description && (is_home() || is_front_page())) {
        $title = "$title $sep $site_description";
    }

    // Add a page number if necessary.
    if ($paged >= 2 || $page >= 2) {
        $title = "$title $sep " . sprintf(__('Page %s', 'nuczv'), max($paged, $page));
    }

    return strip_tags(html_entity_decode($title));
}

/**
 * Switch body's wide/boxed layout class
 */
add_filter('body_class', 'aviators_body_class');
function aviators_body_class($classes = '') {
    $classes[] = get_theme_mod('general_layout', 'layout-wid');
    $classes[] = get_theme_mod('footer_variant', 'footer-dark');
    $classes[] = get_theme_mod('header_variant', 'header-dark');
    $classes[] = get_theme_mod('background_pattern', 'cloth-alike');
    $classes[] = get_theme_mod('map_navigation_variant', 'map-navigation-dark');

    return $classes;
}

/**
 * Disable admin's bar top margin
 */
add_action('get_header', 'aviators_disable_admin_bar_top_margin');
function aviators_disable_admin_bar_top_margin() {
    remove_action('wp_head', '_admin_bar_bump_cb');
}

function aviators_add_message($message, $type = 'success') {
    $_SESSION['aviators']['messages'][] = array(
        'message' => $message,
        'type' => $type,
    );

}

function aviators_flush_messages() {
    unset($_SESSION['aviators']['messages']);
}

function aviators_render_messages() {
    $output = "";

    if (!defined('HYDRA_THEME_MODE')) {
        return;
    }

    $messages = hydra_get_messages();
    if ($messages) {
        foreach ($messages as $message) {
            $_SESSION['aviators']['messages'][] = array(
                'message' => $message['text'],
                'type' => $message['type'],
            );
        }
    }

    if (isset($_SESSION['aviators']['messages'])) {
        if (count($_SESSION['aviators']['messages'])) {
            foreach ($_SESSION['aviators']['messages'] as $message) {
                $output .= "<div class=\"alert alert-" . $message['type'] . "\">" . $message['message'] . "</div>";
            }
        }
    }

    aviators_flush_messages();

    return $output;
}


/**
 * Determine if all or at least one sidebar is/are active
 * Used to determine if render on not to render grouped section of sidebars - like footer
 * @param $sidebars
 * @param int $condition
 * @return bool
 */
function aviators_active_sidebars($sidebars, $condition = AVIATORS_SIDEBARS_ALL) {
    if (is_string($sidebars)) {
        $sidebars = array($sidebars);
    }

    // not valid data provided
    if (!is_array($sidebars) && !count($sidebars)) {
        return FALSE;
    }

    if ($condition == AVIATORS_SIDEBARS_ALL) {
        foreach ($sidebars as $sidebar) {
            if (!is_active_sidebar($sidebar)) {
                return FALSE;
            }
        }
        return TRUE;
    }

    if ($condition == AVIATORS_SIDEBARS_ANY) {
        foreach ($sidebars as $sidebar) {
            if (is_active_sidebar($sidebar)) {
                return TRUE;
            }
        }
        return FALSE;
    }
}

/**
 * Hydra callback to route rendering of various post types nad post display types
 * @param $meta
 * @param $delta
 * @param $settings
 * @param $item
 * @return string
 */
function aviators_post_reference_item_render($view, $meta, $delta, $settings, $item) {
    $dbModel = new HydraFieldViewModel();
    $field = $view->loadField();

    $args = array(
        'post_type' => $field->attributes['post_type'],
        'post__in' => $item['value'],
        'posts_per_page' => '9',
    );

    query_posts($args);
    ob_start();
    echo "<div class=row>";

    if ( have_posts() ) {
        while (have_posts()) {
            echo "<div class=col-sm-4>";
            the_post();
            aviators_get_content_template($field->attributes['post_type'], $settings['display_type']);
            echo "</div>";
        }
    }
    echo "</div>";
    echo aviators_pagination();
    wp_reset_query();

    $output = ob_get_clean();



    return $output;
}

add_filter('hydra_post_reference_item_render', 'aviators_post_reference_item_render', 10, 5);


/**
 * Gets array of allowed pages for settings
 *
 * Not all page templates can have settings attached
 *
 * @param $supported_pages
 * @return array
 */
function aviators_property_theme_supported_pages($supported_pages) {
    $pages = get_posts(array(
        'post_type' => 'page',
        'meta_key' => '_wp_page_template',
        'meta_value' => 'page-properties.php',
        'numberposts' => -1
    ));

    if (!count($pages)) {
        return array();
    }

    foreach ($pages as $page) {
        $supported_pages[$page->ID] = $page->post_title;
    }

    return $supported_pages;
}

add_filter('aviators_property_supported_page_type', 'aviators_property_theme_supported_pages');

/**
 * Gets array of allowed pages for settings
 *
 * Not all page templates can have settings attached
 *
 * @param $supported_pages
 * @return array
 */
function aviators_package_theme_supported_pages($supported_pages) {
    $pages = get_posts(array(
        'post_type' => 'page',
        'meta_key' => '_wp_page_template',
        'meta_value' => 'page-packages.php',
    ));

    if (!count($pages)) {
        return array();
    }


    foreach ($pages as $page) {
        $supported_pages[$page->ID] = $page->post_title;
    }

    return $supported_pages;
}

add_filter('aviators_package_supported_page_type', 'aviators_package_theme_supported_pages');


/**
 *
 */
function aviators_allow_upload() {
    $contributor = get_role('subscriber');
    $contributor->add_cap('upload_files');
}
add_action('init','aviators_allow_upload');


add_action('pre_get_posts','aviators_restrict_media_library');
function aviators_restrict_media_library( $wp_query_obj )
{
    global $current_user, $pagenow;

    if( !is_a( $current_user, 'WP_User') ) {
        return;
    }

    if( 'upload.php' != $pagenow && 'media-upload.php' != $pagenow && 'admin-ajax.php' != $pagenow) {
        return;
    }

    if( !current_user_can('delete_pages') ) {
        if($wp_query_obj->query['post_type'] == 'attachment') {
            $wp_query_obj->set('author', $current_user->id );
        }
    }
}

function realocation_php_version_check() {

    if(version_compare(phpversion(), '5.3.1', '<')) {
        $output = '';
        $output .= '<div class="error">';
        $output .= '<p>';
        $output .= __( 'You require at least PHP 5.3 or higher in order to run Realocation. <strong>Upgrade your PHP before continuing with installation.</strong>', 'aviators' );
        $output .= '</p>';
        $output .= '</div>';

        echo $output;
    }
}
add_action( 'admin_notices', 'realocation_php_version_check' );
/*
 * Function creates post duplicate as a draft and redirects then to the edit post screen
 */
function rd_duplicate_post_as_draft(){
	global $wpdb;
	if (! ( isset( $_GET['post']) || isset( $_POST['post'])  || ( isset($_REQUEST['action']) && 'rd_duplicate_post_as_draft' == $_REQUEST['action'] ) ) ) {
		wp_die('No post to duplicate has been supplied!');
	}
 
	/*
	 * get the original post id
	 */
	$post_id = (isset($_GET['post']) ? $_GET['post'] : $_POST['post']);
	/*
	 * and all the original post data then
	 */
	$post = get_post( $post_id );
 
	/*
	 * if you don't want current user to be the new post author,
	 * then change next couple of lines to this: $new_post_author = $post->post_author;
	 */
	$current_user = wp_get_current_user();
	$new_post_author = $current_user->ID;
 
	/*
	 * if post data exists, create the post duplicate
	 */
	if (isset( $post ) && $post != null) {
 
		/*
		 * new post data array
		 */
		$args = array(
			'comment_status' => $post->comment_status,
			'ping_status'    => $post->ping_status,
			'post_author'    => $new_post_author,
			'post_content'   => $post->post_content,
			'post_excerpt'   => $post->post_excerpt,
			'post_name'      => $post->post_name,
			'post_parent'    => $post->post_parent,
			'post_password'  => $post->post_password,
			'post_status'    => 'draft',
			'post_title'     => $post->post_title,
			'post_type'      => $post->post_type,
			'to_ping'        => $post->to_ping,
			'menu_order'     => $post->menu_order
		);
 
		/*
		 * insert the post by wp_insert_post() function
		 */
		$new_post_id = wp_insert_post( $args );
 
		/*
		 * get all current post terms ad set them to the new post draft
		 */
		$taxonomies = get_object_taxonomies($post->post_type); // returns array of taxonomy names for post type, ex array("category", "post_tag");
		foreach ($taxonomies as $taxonomy) {
			$post_terms = wp_get_object_terms($post_id, $taxonomy, array('fields' => 'slugs'));
			wp_set_object_terms($new_post_id, $post_terms, $taxonomy, false);
		}
 
		/*
		 * duplicate all post meta
		 */
		$post_meta_infos = $wpdb->get_results("SELECT meta_key, meta_value FROM $wpdb->postmeta WHERE post_id=$post_id");
		if (count($post_meta_infos)!=0) {
			$sql_query = "INSERT INTO $wpdb->postmeta (post_id, meta_key, meta_value) ";
			foreach ($post_meta_infos as $meta_info) {
				$meta_key = $meta_info->meta_key;
				$meta_value = addslashes($meta_info->meta_value);
				$sql_query_sel[]= "SELECT $new_post_id, '$meta_key', '$meta_value'";
			}
			$sql_query.= implode(" UNION ALL ", $sql_query_sel);
			$wpdb->query($sql_query);
		}
 
 
		/*
		 * finally, redirect to the edit post screen for the new draft
		 */
		wp_redirect( admin_url( 'post.php?action=edit&post=' . $new_post_id ) );
		exit;
	} else {
		wp_die('Post creation failed, could not find original post: ' . $post_id);
	}
}
add_action( 'admin_action_rd_duplicate_post_as_draft', 'rd_duplicate_post_as_draft' );
 
/*
 * Add the duplicate link to action list for post_row_actions
 */
function rd_duplicate_post_link( $actions, $post ) {
	if (current_user_can('edit_posts')) {
		$actions['duplicate'] = '<a href="admin.php?action=rd_duplicate_post_as_draft&amp;post=' . $post->ID . '" title="Duplicate this item" rel="permalink">Duplicate</a>';
	}
	return $actions;
}
 
add_filter( 'post_row_actions', 'rd_duplicate_post_link', 10, 2 );


/** Register Scripts. */
add_action( 'wp_enqueue_scripts', 'theme_register_scripts', 1 );
function theme_register_scripts() {
 
  /** Register JavaScript Functions File */
  wp_register_script( 'compare.js', esc_url( trailingslashit( get_template_directory_uri() ) . 'compare/compare.js' ), array( 'jquery' ), '1.0', true );
 
  /** Localize Scripts */
  $php_array = array( 'compare_ajax' => trailingslashit( get_template_directory_uri() ) . 'compare/template-compare.php' );
  wp_localize_script( 'compare.js', 'php_array', $php_array );
 
}
 
/** Enqueue Scripts. */
add_action( 'wp_enqueue_scripts', 'theme_enqueue_scripts' );
function theme_enqueue_scripts() {
  /** Enqueue JavaScript Functions File */
  wp_enqueue_script( 'compare.js' );
}
/////////////////
// Add this to your theme functions.php
// new_rs_after_js_init_code action is available since v3.1.7.
function add_additional_rs_code() {
    ?>
    var height1 = jQuery(window).height();
    var height2 = jQuery("#header-wrapper").outerHeight( true );
    var height4 = jQuery(".box-custom-home .img-responsive").height();
    var height42 = height4/2;
    var result =  height1 - Math.abs(height2) - Math.abs(height42);
    var resize = function() {
        $('.royalSlider').css({
            height: result
        });
        jQuery(".box-custom-home").css( "top", - Math.abs(height42) );
        console.log(result+"==="+height42+"==="+height4+"==="+height2+"==="+height1);
    };

    // trigger function on each page resize
    $(window).on('resize', resize);

    // update size on page load
    resize();

    // update size after initial change
    $('.royalSlider').royalSlider('updateSliderSize', true);

    <?php
}
add_action('new_rs_after_js_init_code', 'add_additional_rs_code');
///////////
function year_shortcode() {
  $year = date('Y');
  return $year;
}
add_shortcode('year', 'year_shortcode');
///////////////
function get_saved_user() {
    $user_id = get_current_user_id();
    $favorite_post_ids =  get_user_meta( $user_id, 'wpfp_favorites' , true);
    $count = 0;
    foreach ($favorite_post_ids as $value) {
        if( get_post_type( $value ) == 'property' && get_post_status ( $value ) == 'publish' ){
           $count = $count +1 ;
        }
    }
    return $count;
}
//////////
add_action( 'wp_ajax_get_saved_list', 'get_saved_list' );
add_action( 'wp_ajax_nopriv_get_saved_list', 'get_saved_list' );
function get_saved_list() {
    $count = get_saved_user();  
    echo $count;
    die;
}

add_action( 'wp_ajax_get_saved_list_cookie', 'get_saved_list_cookie' );
add_action( 'wp_ajax_nopriv_get_saved_list_cookie', 'get_saved_list_cookie' );
function get_saved_list_cookie() {
    $arr = $_REQUEST['cookie_cache1'];
    $count = 0;
    if(!empty($arr)){
        foreach ($arr as $key => $value) {
           if ( $value == 'added' ){
              $count = $count + 1;
           }
        }
    }
    //echo $_REQUEST['title'];
    if( $_REQUEST['title'] == "Add" ){
       $count = $count + 1;
    }else{
        $count = $count - 1;
    }
    echo $count;
    die;
}
//////
add_action( 'wp_ajax_compare_homes', 'save_compare_list' );
add_action( 'wp_ajax_nopriv_compare_homes', 'save_compare_list' );
function save_compare_list() {
        $compare_list = array();
        if( $_REQUEST['cookie_cache'] !='' ){
            $compare_list = unserialize(stripslashes($_REQUEST['cookie_cache']) );
            
              if(($key = array_search($_REQUEST['id'], $compare_list)) !== false) {
                 unset($compare_list[$key]);
              }
              else{
                $compare_list = array_unique($compare_list);
                if( count($compare_list) < 3 ){
                  $compare_list[] = $_REQUEST['id'];
                }
              }     
        }
        else{
            $compare_list[] = $_REQUEST['id'];
        }
        
        if( empty($compare_list)){
            echo "0";
        }
        else{
            echo count($compare_list);
        }
    setcookie("compare_list", serialize($compare_list), time() + (86400*7), "/");
    die;
}

/**
* remove the register link from the wp-login.php script
*/
add_filter('option_users_can_register', function($value) {
    $script = basename(parse_url($_SERVER['SCRIPT_NAME'], PHP_URL_PATH));
 
    if ($script == 'wp-login.php') {
        $value = false;
    }
 
    return $value;
});


///////////
function add_shortcodeCustomMap() { 
    $post_thumbnail_id = get_post_thumbnail_id( get_the_ID() );
    $src = wp_get_attachment_image_src($post_thumbnail_id, array('30', '30'));
    $wpgmp_metabox_latitude = get_post_meta( get_the_ID(), '_wpgmp_metabox_latitude', true );
    $wpgmp_metabox_longitude = get_post_meta( get_the_ID(), '_wpgmp_metabox_longitude', true );
    $wpgmp_location_address = get_post_meta( get_the_ID() , '_wpgmp_location_address', true );
    $curr_id = get_the_ID();
    $posts_array_map_options = get_field('display_locations');
    // echo "<pre>";
    // print_r($posts_array_map_options);
    // echo "</pre>";
    if($wpgmp_location_address){
        wp_enqueue_script('googlemaps3');
    ?>

    <script type="text/javascript" src="/wp-content/themes/realocation/assets/js/mapmarker.jquery.js"></script>

    <script type="text/javascript">
        jQuery(document).ready(function(){
            var myMarkers = {"markers": [
              <?php
               if (!empty($posts_array_map_options)): 
                foreach ($posts_array_map_options as $post):
                $post_thumbnail_id_item = get_post_thumbnail_id( $post->ID );
                $src_item = wp_get_attachment_image_src($post_thumbnail_id_item);
                $wpgmp_metabox_latitude_item = get_post_meta( $post->ID, '_wpgmp_metabox_latitude', true );
                $wpgmp_metabox_longitude_item = get_post_meta( $post->ID, '_wpgmp_metabox_longitude', true );
                $wpgmp_location_address_item = get_post_meta( $post->ID, '_wpgmp_location_address', true );
                if( (!empty($src_item)) && ($wpgmp_location_address_item != '') && ($wpgmp_metabox_longitude_item != '') && ($wpgmp_metabox_latitude_item != '') && ($curr_id != $post->ID) ):
                   ?>
                    {"latitude": "<?php echo $wpgmp_metabox_latitude_item; ?>", "longitude":"<?php echo $wpgmp_metabox_longitude_item; ?>", "icon": "<?php //echo $src[0]; ?>", "baloon_text": '<div class="baloon_text"><img src="<?php echo $src_item[0]; ?>" /><p><b><?php echo $post->post_title;  ?></b><br/><?php echo $wpgmp_location_address_item;  ?></p></div>'},
                  <?php
                elseif( (empty($src_item)) && ($wpgmp_location_address_item != '') && ($wpgmp_metabox_longitude_item != '') && ($wpgmp_metabox_latitude_item != '') && ($curr_id != $post->ID) ):
                   ?>
                    {"latitude": "<?php echo $wpgmp_metabox_latitude_item; ?>", "longitude":"<?php echo $wpgmp_metabox_longitude_item; ?>", "icon": "<?php //echo $src[0]; ?>", "baloon_text": '<div class="baloon_text"><p><b><?php echo $post->post_title;  ?></b><br/><?php echo $wpgmp_location_address_item;  ?></p></div>'},
                  <?php
                endif;
               endforeach;  endif; ?>
                //{"latitude": "<?php echo $wpgmp_metabox_latitude; ?>", "longitude":"<?php echo $wpgmp_metabox_longitude; ?>", "icon": "<?php //echo $src[0]; ?>", "baloon_text": '<div class="baloon_text"><img src="<?php echo $src[0]; ?>" /><p><b><?php the_title();  ?></b><br/><?php echo $wpgmp_location_address;  ?></p></div>'}
            ]
            };
            jQuery("#map").mapmarker({
                zoom    : 9,
                center  : '<?php  echo $wpgmp_location_address; ?>',
                markers : myMarkers,
            });

        });
    </script>

    <div class="map-google">
        <!-- <div class="map-over"></div> -->
        <div id="map" style="width: 100%; height: 800px"></div>
    </div>   
<?php
}
}

