<?php
/**
 * Template Name: Compare Homes
 */
?>
<?php get_header(); 
if(isset($_COOKIE['compare_list'])) {
    $compare_list = unserialize(stripslashes($_COOKIE['compare_list']) );
}
$count = 0;
?>
<script type="text/javascript" src="/wp-content/themes/realocation/fancybox/fancybox.js"></script>
<link rel="stylesheet" type="text/css" href="/wp-content/themes/realocation/fancybox/fancybox.css" media="screen">
    <div id="main-content" class="col-md-12 col-sm-12">
        <?php if (dynamic_sidebar('content-top')) : ?><?php endif; ?>
        <h1 <?php if ($display == 'isotope'): ?>class="center"<?php endif; ?>>
            <?php the_title(); ?>
            <?php aviators_edit_post_link(); ?>
        </h1>
        <?php wp_reset_query(); ?>
        <?php if (dynamic_sidebar('content-bottom')) : ?><?php endif; ?>
        <div id="compare-homes">
        <?php 
        
        if(isset($_COOKIE['compare_list'])) {
             foreach( $compare_list as $id ){ 
             $count++;
            ?>
              <div class="box select_<?php echo $count;?>">
                           <div class="box-top">
                                <select class="select-wrapper" id="select_<?php echo $count;?>" style="width: 100%;">
                                    <option value="0">Select Home</option>
                                    <?php
                                      //$values = get_field( 'select_'.$count );
                                       $values = get_field( 'select_homes' );
                                     if($values){ 
                                        foreach($values as $value):?>
                                        <?php if($value->post_status=='publish'):?>
                                           <option value="<?php print $value->ID; ?>" <?php if( $value->ID == $id ){ echo "selected"; } ;?>><?php echo $value->post_title;?></option>
                                        <?php endif; ?>
                                    <?php endforeach; 
                                    } ?>
                                <select>
                            </div>
                            <div class="box-content">
                                <div class="post-title">
                                  <h3><?php echo get_the_title($id); ?></h3>
                                </div>
                                <div class="post-image">
                                    <?php $image_top = wp_get_attachment_image_src( get_post_thumbnail_id( $id ), 'large' );?>
                                    <?php if(!empty($image_top)):?>
                                        <div class="image-box">
                                            <img src="<?php print $image_top[0];?>">
                                        </div>
                                    <?php endif ?>
                                </div>
                                <div class="col-md-12 post-content">
                                    <?php print hydra_render_field($id, 'hf_property_short_description', 'detail'); ?>
                                    <?php $url=get_permalink($id);?>
                                    <?php if (!empty($url)):?>
                                        <a class="read_more" target="_blank" href="<?php print $url;?>">More info</a>
                                    <?php endif ?>
                                </div>
                                <div class="col-md-12 post-data">
                                <ul class="hf_property_overview_custom room">
                                <?php 
                                $key_1_value = get_post_meta( $id , 'hf_property_bedrooms', true );
                                if( !empty($key_1_value)){
                                    echo '<li class="beds">'.$key_1_value['items'][0]['value'].'</li>';
                                }
                                $key_2_value = get_post_meta( $id , 'hf_property_bathrooms', true );
                                if( !empty($key_2_value)){
                                    echo '<li class="baths">'.$key_2_value['items'][0]['value'].'</li>';
                                }
                                $key_3_value = get_post_meta( $id , 'hf_property_storeys', true );
                                if( !empty($key_3_value)){
                                    echo '<li class="storeys">'.$key_3_value['items'][0]['value'].'</li>';
                                }
                                $key_4_value = get_post_meta( $id , 'hf_property_car_spaces', true );
                                if( !empty($key_4_value)){
                                    echo '<li class="garages">'.$key_4_value['items'][0]['value'].'</li>';
                                }
                                $key_5_value = get_post_meta( $id , 'hf_property_area', true );
                                if( !empty($key_5_value)){
                                    echo '<li class="area">'.$key_5_value['items'][0]['value'].'</li>';
                                }
                                ?>
                                </ul>
                                <?php
                                if( get_field('room_dimensions',$id) ){
                                    echo get_field('room_dimensions',$id);
                                }
                                if( get_field('home_dimensions',$id) ){
                                    echo get_field('home_dimensions',$id);
                                }
                                 if( get_field('floor_plan',$id) ){ 
                                    $image = get_field('floor_plan',$id) ;    ?>
                                        <div class="overview-item">
                                            <a class="floor-plan fancybox"
                                               href="<?php echo $image['url'];?>">
                                                <?php print wp_get_attachment_image($image['id'], array(262, 300)); ?>
                                            </a>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
              
            <?php  }
            wp_reset_postdata(); 
         }
         for( $i = $count+1 ; $i<4; $i++ ) {
             //$values = get_field( 'select_'.$i );
             $values = get_field( 'select_homes' );  ?>
                        <div class="box select_<?php echo $i;?>">
                            <div class="box-top">
                                <select class="select-wrapper" id="select_<?php echo $i;?>" style="width: 100%;">
                                    <option value="0" selected>Select Home</option>
                                    <?php foreach($values as $value):?>
                                        <?php if($value->post_status=='publish'):?>
                                           <option value="<?php print $value->ID; ?>"><?php print $value->post_title;?></option>
                                        <?php endif ?>
                                    <?php endforeach; ?>
                                <select>
                            </div>
                            <div class="box-content">
                            </div>
                        </div>
        <?php  }  ?>
            </div>
    </div><!-- /#main-content -->
 <script type="text/javascript">
    jQuery(function(){
        jQuery(".post-data a.fancybox").fancybox();
    })
</script>
<?php get_footer(); ?>