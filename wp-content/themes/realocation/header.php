<?php do_action('aviators_pre_render'); ?>
<!DOCTYPE html>
<html <?php language_attributes() ?>>
<head>

	<!-- Google Tag Manager -->

	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push(

			{'gtm.start': new Date().getTime(),event:'gtm.js'}
		);var f=d.getElementsByTagName(s)[0],

			j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=

			'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);

		})(window,document,'script','dataLayer','GTM-5B7H266');</script>

	<!-- End Google Tag Manager -->


    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="">

    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

    <?php wp_head(); ?>

    <?php if ( is_singular() ) wp_enqueue_script( "comment-reply" ); ?>
    <title><?php echo wp_title('|', FALSE, 'right'); ?></title>

    <script>
        jQuery(function(){
            jQuery( "#submit-search" ).click(function(){
                var element  = jQuery( "#s" );
                element.addClass('show');
                element.animate({
                    padding: '5',
                    width: "200"
                }, 1000, function() {
                    // Animation complete.
                });
            });
        });
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/js-cookie/2.1.3/js.cookie.js"></script>

</head>

<body <?php body_class(); ?> >


<!-- Google Tag Manager (noscript) -->

<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5B7H266"
 height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

<!-- End Google Tag Manager (noscript) -->

<?php if(get_theme_mod('general_enable_customizer', 1)): ?>
    <?php aviators_get_template('template', 'palette'); ?>
<?php endif; ?>
<?php 
global $parent_page_id;
$parent_page_id = get_the_ID();
?>
<div id="wrapper">
    <div id="header-wrapper">
        <div id="header">
            <div id="header-inner">
                <?php if ( get_theme_mod('general_topbar_is_enabled') ) : ?>
                    <?php require_once 'templates/header-bar.php'; ?>
                <?php endif; ?>
                <?php require_once 'templates/header-top.php'; ?>
                <?php //require_once 'templates/header-navigation.php'; ?>
            </div><!-- /.header-inner -->
        </div><!-- /#header -->
    </div><!-- /#header-wrapper -->

    <div id="main-wrapper">
        <div id="main">
            <div id="main-inner">
                <?php
                if(!is_singular( 'property' )): ?>
                    <div class="slide-home">
                        <?php 
                        add_shortcodeCustomMap(); 
                        dynamic_sidebar( 'top-fullwidth' ); 
                        ?>
                    </div>
                <?php endif; ?>
                <?php if (is_singular(array('property'))): ?>
                    <?php
                    wp_enqueue_script('googlemaps3');
                    wp_enqueue_script('clusterer');
                    wp_enqueue_script('infobox');
                    ?>
                    <?php $mapPosition = get_post_meta(get_the_ID(), 'hf_property_map', TRUE); ?>
                    <?php if (isset($mapPosition['items'][0])): ?>
                        <?php if (!empty($mapPosition['items'][0]['latitude']) && !empty($mapPosition['items'][0]['longitude'])) : ?>
                            <div id="map-property">
                            </div><!-- /#map-property -->
                        <?php endif; ?>
                    <?php endif; ?>
                    <?php add_action('aviators_footer_map_detail', 'aviators_properties_map_detail'); ?>
                <?php endif; ?>

                <?php $type_property = get_post_meta( get_the_ID(), 'hf_property_type_0_value' );?>

                <div class="<?php if ($type_property[0]==273) echo 'property-detail';  ?> container">
                    <?php dynamic_sidebar( 'top' ); ?>

                    <?php echo aviators_render_messages(); ?>

                    <div class="block-content block-content-small-padding">
                        <div class="block-content-inner">
                            <div class="row">