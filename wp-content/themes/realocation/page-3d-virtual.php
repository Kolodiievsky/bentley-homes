<?php
/**
 * Template Name: 3D Virtual
 */
?>

<?php get_header(); ?>


    <div class="alert-no-margin">
        <?php echo aviators_render_messages(); ?>
    </div><!-- /.aler-no-margin -->

    <?php if (is_active_sidebar('top')) : ?>
        <?php if (dynamic_sidebar('top')) : ?><?php endif; ?>
    <?php endif ?>

    <div class="container">
        <div class="row">
            <div id="main-content" class="empty col-md-12 col-sm-12">
                <?php echo aviators_render_messages(); ?>
                <?php if (dynamic_sidebar('content-top')) : ?><?php endif; ?>

            <!--<?php  // while (have_posts()) : the_post(); ?>
                    <?php // get_template_part('content', get_post_format()); ?>
                <?php // endwhile; ?>-->
          <div class="post-5548 page type-page status-publish hentry publish-tours
">
             <div class="clearfix">
              <div class="col-md-12">
                <h2 class="page-title">3D Virtual Tours – Homes on Display</h2>
                <div class="content-wrapper">
                <?php   if( have_rows('3d_content') ): ?>
                  <div class="content">
                     <?php while( have_rows('3d_content') ): the_row(); 
                         $heading = get_sub_field('tittle_heading'); 
                         $Subheading = get_sub_field('sub_heading'); 
                         $image = get_sub_field('image'); 
                         $image_size = 'thumbnail';
                         $image_url = $image['sizes'][$image_size];
                         $link = get_sub_field('link_image');
                         $class = get_sub_field('add_the_class');

                      ?>
                    <div class="ezcol <?php echo $class ; ?>">
                      <h3><?php echo $heading ;?></h3>
                      <p><?php echo  $Subheading ;?></p>
                      <a href="<?php echo $link ; ?>" target="_blank">
                        <img class="alignnone wp-image-5892"  src="<?php echo $image_url ; ?>" alt="<?php echo $image['alt'] ?>" />
                      </a>
                    </div> 
                    <?php endwhile; ?>  
                  </div>
                  <?php endif; ?>
               </div>
              </div>
            </div>
           </div>
                <?php if (dynamic_sidebar('content-bottom')) : ?><?php endif; ?>  
            </div>
            <!-- /#main-content -->
        </div>
        <!-- /.row -->
    </div><!-- /.container -->


<?php get_footer(); ?>