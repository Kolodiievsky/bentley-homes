<?php
/**
 * Template Name: Product detail
 */
?>

<?php get_header(); ?>

<div id="content">
    <?php if ( is_active_sidebar( 'top' ) ) : ?>
        <?php if ( dynamic_sidebar( 'top' ) ) : ?><?php endif; ?>
    <?php endif ?>

    <div class="container">
        <div class="row">
            <div id="main" class="col-md-12">

            </div><!-- /#main -->
        </div><!-- /.row -->
    </div><!-- /.container -->


</div><!-- /#content -->

<?php get_footer(); ?>