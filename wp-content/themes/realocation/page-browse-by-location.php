<?php
/**
 * Template Name: Browse by Location Page
 */
?>
<?php get_header(); ?>
<div id="main-content" class=" <?php if (is_active_sidebar('sidebar-1')) : ?>col-md-9 col-sm-9<?php else : ?>col-md-12 col-sm-12<?php endif; ?>">
    <?php dynamic_sidebar( 'content-top' ); ?>

    <?php while ( have_posts() ) : the_post(); ?>
        <div <?php post_class() ?>>
                <div class="col-md-<?php if ( !empty($thumbnail_url) ) : ?>6<?php else : ?>10<?php endif; ?>">
                    <div class="content-wrapper">
                        <div class="content">
                                <?php the_content(); ?>
                                <div class="list_by_loaction">
                                    <ul>
                                     <?php
                                       if( have_rows('suburbs_details') ):
                                          while ( have_rows('suburbs_details') ) : the_row();
                                              echo "<li>";
                                              echo "<a href='".get_site_url()."/house-and-land/browse-our-hl-packages/?suburbs_filter=".get_sub_field('name')."&price_filter=100000%3B800000&submit=Search'>";
                                              echo "<span class='number'>".get_sub_field('number')."</span>";
                                              echo "<span class='title'>".get_sub_field('name')."</span>";
                                              echo "</a>";
                                              echo "</li>";
                                          endwhile;
                                        endif;
                                     ?>
                                    </ul>
                                </div>
                        </div><!-- /.content -->
                    </div><!-- /.content-wrapper -->
                </div><!-- /.col-md-8 -->
        </div>
    <?php endwhile; ?>

    <?php dynamic_sidebar( 'content-bottom' ); ?>
</div><!-- /#main -->

<?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
    <div class="sidebar col-md-3 col-sm-3">
        <?php dynamic_sidebar( 'sidebar-1' ); ?>
    </div><!-- /.sidebar -->
<?php endif; ?>

<?php get_footer(); ?>