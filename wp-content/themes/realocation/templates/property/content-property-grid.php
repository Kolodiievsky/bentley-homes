<div class="property-box">
    <div class="property-box-inner">
           <?php
            //if ( is_user_logged_in() ) {
                if (function_exists('wpfp_link')) { wpfp_link(); }
            // } else {
            //     global $parent_page_id;
            //     $id = $parent_page_id;
            //     echo '<a href="'.get_site_url().'/login/?id='.$id.'" class="save">Log in</a>';
            // }
            if(!isset($_COOKIE['compare_list'])) {
                $class='';
            }
            else{
                 $compare_list = unserialize(stripslashes($_COOKIE['compare_list']) );
                  if(($key = array_search( get_the_ID(), $compare_list)) !== false) {
                     $class='active_compare';
                  }
            } 
            echo "<a class='save_compare ".$class."' href='#' data-id='".get_the_ID()."'><i class='fa fa-exchange' aria-hidden='true'></i></a>";
            ?>
        <div class="inner_custom">
         <a href="<?php echo get_permalink(); ?>">
            <div class="property-box-header">
                <h3 class="property-box-title"><?php the_title(); ?></h3>
                <div class="property-box-subtitle"><?php print hydra_render_field(get_the_ID(), 'location', 'grid'); ?></div>
            </div><!-- /.property-box-header -->
           </a>
            <div class="property-box-picture">
                <div class="property-box-picture-inner">
                     <a href="<?php echo get_permalink(); ?>" class="property-box-picture-target">
                          <img src="<?php echo aviators_get_featured_image(get_the_ID(), 320, 190); ?>" alt="<?php the_title(); ?>">
                      </a>
                </div><!-- /.property-picture-inner -->
            </div><!-- /.property-picture -->
            <a target="_blank" href="<?php echo get_permalink(); ?>">
            <div class="property-box-meta">
                <?php echo hydra_render_group(get_the_ID(), 'meta', 'grid'); ?>
            </div><!-- /.property-box-meta -->
            </a>
        
        </div>
    </div><!-- /.property-box-inner -->
</div><!-- /.property-box -->