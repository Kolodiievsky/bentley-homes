<script type="text/javascript" src="<?php echo get_bloginfo('template_directory');?>/fancybox/fancybox.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_directory');?>/fancybox/fancybox.css" media="screen">
<script type="text/javascript">
    jQuery(function(){
        jQuery("a.fancybox").fancybox();
    })
</script>
<style>
	.vl .label {
		display: none;
	}
	.vl .field-value {
		font-size: 20px;
		line-height: 22px;
	}
</style>
<?php $meta = get_post_meta( get_the_ID() ); ?>
<div class="property-detail">
    <div class="row">
        <div class="col-md-12 box-title">
            <h1 class="property-detail-title"><?php the_title(); ?></h1>
            <p class="subde"></p>
            <div class="property-detail-subtitle">
                <?php print hydra_render_field(get_the_ID(), 'location', 'detail'); ?>
                <?php print hydra_render_field(get_the_ID(), 'text_price', 'detail'); ?>
                <?php print hydra_render_field(get_the_ID(), 'price', 'detail'); ?>
            </div>
        </div>
        <!-- /.header-title -->
    </div>
    <!-- /.row -->

  <div class="slide-product">
      <div class="slide-gallery">
          <div class="scron-left"></div>
          <div class="scron-right"></div>
          <?php  print hydra_render_field(get_the_ID(), 'gallery', 'detail'); ?>
      </div>

      <div class="carousel-box container">
          <?php echo  str_replace("flexslider_gallery", "slide-carousel" ,hydra_render_field(get_the_ID(), 'gallery', 'detail')); ?>
      </div>
      <div class="clearfix"></div>
  </div>

    <!-- /.row -->

    <div class="container">
        <div class="short-description">
            <h2><?php if( get_field('sub_heading',get_the_ID()) ){ echo get_field('sub_heading',get_the_ID()); } ?></h2>
            <div class="col-md-6">
                <?php print hydra_render_field(get_the_ID(), 'hf_property_short_description', 'detail'); ?>
            </div>
        </div>
    </div>

    <?php if (isset($meta['floor_plan'][0]) && !empty($meta['floor_plan'][0])): ?>
        <div class="overview-item custom-floor">
            <div class="container">
                <div class="title-overview-item">
                    <h3 >Floor plan</h3>
                    <p>Select a floorplan to view details</p>
                    <span class="hr-box"></span>
                </div>
                <!-- <div class="button1"><a href="#">GROUND</a>
                    <a href="#">LEVEL 1</a></div> -->


                <div class="col-md-6 box-left">
                    <h3 class="title-post"><?php the_title();  ?></h3>
                    <span class="hr-box"></span>
                    <div class="room">
                        <ul>
                            <li class="beds vl">
								<?php echo hydra_render_field(get_the_ID(), 'bedrooms', "detail")?>
                            </li>
                            <li class="baths vl">
								<?php echo hydra_render_field(get_the_ID(), 'bathrooms', "detail")?>
                            </li>
                            <li class="garages vl">
								<?php echo hydra_render_field(get_the_ID(), 'car_spaces', "detail")?>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>

                    <div class="col-md-6 mix-property">MIN. BLOCK WIDTH<br/>
                        <?php 
                        if( get_field('min_block_width',get_the_ID()) ){
                            echo get_field('min_block_width',get_the_ID())." m";
                        }
                        else{
                            echo "10.75 m";
                        }
                        ?></div>
                    <div class="col-md-6 lenght-property">
                        MIN. BLOCK LENGTH<br/>
                        <?php 
                        if( get_field('overall_length',get_the_ID()) ){
                            echo get_field('overall_length',get_the_ID())." m";
                        }
                        else{
                            echo "20.00 m";
                        }
                        ?>
                    </div>
                    <div class="clearfix"></div>
                    <div class="info-box1 ">
                    <?php $key_area_value = get_post_meta( get_the_ID() , 'hf_property_area', true ); ?>
                        <p>TOTAL SIZE - <?php echo $key_area_value['items'][0]['value']."m2 / "; ?><?php 
                        if( get_field('area_per_sq',get_the_ID()) ){
                            echo get_field('area_per_sq',get_the_ID())." sq";
                        }
                        else{
                            echo "28.39 sq";
                        }
                        ?></p>
                        
                        <p>FEATURES</p>
                        <div class="features">
                        <?php if( get_field('features',get_the_ID()) ){
                            echo get_field('features',get_the_ID());
                        } ?>
                        </div>
                    </div>
                    <div class="price-box">
                        FROM<br/>
                        <div><?php print hydra_render_field(get_the_ID(), 'price', 'row'); ?></div>
                    </div>
                </div>
                <div class="col-md-6 box-right">

                    <a class="floor-plan fancybox" href="<?php print wp_get_attachment_image_src($meta['floor_plan'][0], 'full')[0];?>" title="<?php the_title(); ?> - Floor plan">
                        <?php print wp_get_attachment_image( $meta['floor_plan'][0], array( 400, 600), array('class' => 'img-responsive') );?>
                    </a>

                </div>
                <div class="clearfix"></div>



            </div>

        </div>
    <?php endif; ?>
    <div class="container">
        <div class="content-tab">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#overview" aria-controls="overview" role="tab" data-toggle="tab">OVERVIEW</a></li>
                <?php 
                $file1 = get_field('inclusions');

                if( $file1 ): ?>
                    <li role="presentation" class="inclusion"><a href="<?php echo $file1['url']; ?>" target="_blank">Inclusions</a></li>

                <?php endif; ?>
                                <?php 
                                $file2 = get_field('brochure');

                if( $file2 ): ?>
                    <li role="presentation" class="brochure"><a href="<?php echo $file2['url']; ?>" target="_blank" >Brochure</a></li>

                <?php endif; ?>
                
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="overview">
                    <h4 class="price">Price from: <?php print hydra_render_field(get_the_ID(), 'price', 'row'); ?></h4>
                    <div class="room_dimensions overview_table">
                    <?php  if( get_field('room_dimensions',get_the_ID()) ){
                            echo get_field('room_dimensions',get_the_ID());
                        } ?>
                    </div>
                    <div class="home_dimensions overview_table">
                    <?php  if( get_field('home_dimensions',get_the_ID()) ){
                            echo get_field('home_dimensions',get_the_ID());
                        } ?>
                    </div>
                   
                </div>          
            </div>
        </div>

    </div>


    <div class="container">
        <?php $amenities = hydra_render_field(get_the_ID(), 'amenities', 'detail'); ?>
        <?php if ($amenities): ?>
            <div class="row">
                <div class="col-md-12"><h2><?php echo __('Amenities', 'aviators'); ?></h2></div>
                <div class="property-detail-amenities">
                    <?php print $amenities; ?>
                </div>
            </div>
        <?php endif; ?>




        <div class="facade-option">
            <h2>FACADE OPTIONS</h2>
            <p>Select a facade below to view details</p>
            <span class="hr-box"></span>
            <?php
            $posts_array_facade_options = get_field('facade_options');
            if (!empty($posts_array_facade_options)): ?>
                 <div class="col-md-10 slide-house">
                    <div id="carousel_option" class="carousel_post flexslider">
                        <ul class="slides">
                            <?php foreach ($posts_array_facade_options as $post):
                                $post_thumbnail_id = get_post_thumbnail_id( $post->ID );
                                $img = wp_get_attachment_image( $post_thumbnail_id, 'facade-thumb' , "", array(  'alt' => $post->post_title) );
                                $feat_image_url = wp_get_attachment_url( $post_thumbnail_id );
                                if ($img != ''):
                                    ?>
                                    <li class="itemm">
                                        <a rel="gallery_slides" title="<?php echo $post->post_title; ?>" href="<?php echo $feat_image_url; ?>" class="fancybox image"><?php echo $img;   ?></a>
                                        <span>
                                            <a title="<?php echo $post->post_title; ?>" href="<?php echo get_permalink($post->ID); ?>"><?php echo $post->post_title; ?></a></span>

                                    </li>
                                    <?php
                                endif;
                            endforeach; ?>

                        </ul>
                    </div>
                </div>
            <?php endif; ?>
        </div>
  </div>


    <div class="display-location">
        <h3>DISPLAY LOCATIONS</h3>
        <p>Find your nearest Display Village</p>
    </div>

    <?php
    $post_thumbnail_id = get_post_thumbnail_id( get_the_ID() );
    $src = wp_get_attachment_image_src($post_thumbnail_id, array('30', '30'));
    $wpgmp_metabox_latitude = get_post_meta( get_the_ID(), '_wpgmp_metabox_latitude', true );
    $wpgmp_metabox_longitude = get_post_meta( get_the_ID(), '_wpgmp_metabox_longitude', true );
    $wpgmp_location_address = get_post_meta( get_the_ID() , '_wpgmp_location_address', true );
    $curr_id = get_the_ID();
    $posts_array_map_options = get_field('map_locations');
    // echo "<pre>";
    // print_r($posts_array_map_options);
    // echo "</pre>";

    ?>


    <script type="text/javascript" src="/wp-content/themes/realocation/assets/js/mapmarker.jquery.js"></script>
    <?php if ( $wpgmp_location_address ){ ?>
    <script type="text/javascript">
        jQuery(document).ready(function(){
            var myMarkers = {"markers": [
              <?php
               if (!empty($posts_array_map_options)): 
                foreach ($posts_array_map_options as $post):
                $post_thumbnail_id_item = get_post_thumbnail_id( $post->ID );
                $src_item = wp_get_attachment_image_src($post_thumbnail_id_item);
                $wpgmp_metabox_latitude_item = get_post_meta( $post->ID, '_wpgmp_metabox_latitude', true );
                $wpgmp_metabox_longitude_item = get_post_meta( $post->ID, '_wpgmp_metabox_longitude', true );
                $wpgmp_location_address_item = get_post_meta( $post->ID, '_wpgmp_location_address', true );
                if( (!empty($src_item)) && ($wpgmp_location_address_item != '') && ($wpgmp_metabox_longitude_item != '') && ($wpgmp_metabox_latitude_item != '') && ($curr_id != $post->ID) ):
                   ?>
                    {"latitude": "<?php echo $wpgmp_metabox_latitude_item; ?>", "longitude":"<?php echo $wpgmp_metabox_longitude_item; ?>", "icon": "<?php //echo $src[0]; ?>", "baloon_text": '<div class="baloon_text"><img src="<?php echo $src_item[0]; ?>" /><p><b><?php echo $post->post_title;  ?></b><br/><?php echo $wpgmp_location_address_item;  ?></p></div>'},
                  <?php
                elseif( (empty($src_item)) && ($wpgmp_location_address_item != '') && ($wpgmp_metabox_longitude_item != '') && ($wpgmp_metabox_latitude_item != '') && ($curr_id != $post->ID) ):
                   ?>
                    {"latitude": "<?php echo $wpgmp_metabox_latitude_item; ?>", "longitude":"<?php echo $wpgmp_metabox_longitude_item; ?>", "icon": "<?php //echo $src[0]; ?>", "baloon_text": '<div class="baloon_text"><p><b><?php echo $post->post_title;  ?></b><br/><?php echo $wpgmp_location_address_item;  ?></p></div>'},
                  <?php
                endif;
               endforeach;  endif; ?>
                {"latitude": "<?php echo $wpgmp_metabox_latitude; ?>", "longitude":"<?php echo $wpgmp_metabox_longitude; ?>", "icon": "<?php //echo $src[0]; ?>", "baloon_text": '<div class="baloon_text"><img src="<?php echo $src[0]; ?>" /><p><b><?php the_title();  ?></b><br/><?php echo $wpgmp_location_address;  ?></p></div>'}
            ]
            };
            jQuery("#map").mapmarker({
                zoom	: 8,
                center	: '<?php  echo $wpgmp_location_address; ?>',
                markers	: myMarkers,
            });

        });
    </script>
    <?php }else{
    $custom_address = ''; ?>
    <script type="text/javascript">
        jQuery(document).ready(function(){
            var myMarkers = {"markers": [
              <?php
               if (!empty($posts_array_map_options)): 
                foreach ($posts_array_map_options as $post):
                $post_thumbnail_id_item = get_post_thumbnail_id( $post->ID );
                $src_item = wp_get_attachment_image_src($post_thumbnail_id_item);
                $wpgmp_metabox_latitude_item = get_post_meta( $post->ID, '_wpgmp_metabox_latitude', true );
                $wpgmp_metabox_longitude_item = get_post_meta( $post->ID, '_wpgmp_metabox_longitude', true );
                $wpgmp_location_address_item = get_post_meta( $post->ID, '_wpgmp_location_address', true );
                if( (!empty($src_item)) && ($wpgmp_location_address_item != '') && ($wpgmp_metabox_longitude_item != '') && ($wpgmp_metabox_latitude_item != '') && ($curr_id != $post->ID) ):
                   $custom_address = $wpgmp_location_address_item;
                   ?>
                    {"latitude": "<?php echo $wpgmp_metabox_latitude_item; ?>", "longitude":"<?php echo $wpgmp_metabox_longitude_item; ?>", "icon": "<?php //echo $src[0]; ?>", "baloon_text": '<div class="baloon_text"><img src="<?php echo $src_item[0]; ?>" /><p><b><?php echo $post->post_title;  ?></b><br/><?php echo $wpgmp_location_address_item;  ?></p></div>'},
                  <?php
                elseif( (empty($src_item)) && ($wpgmp_location_address_item != '') && ($wpgmp_metabox_longitude_item != '') && ($wpgmp_metabox_latitude_item != '') && ($curr_id != $post->ID) ):
                   ?>
                    {"latitude": "<?php echo $wpgmp_metabox_latitude_item; ?>", "longitude":"<?php echo $wpgmp_metabox_longitude_item; ?>", "icon": "<?php //echo $src[0]; ?>", "baloon_text": '<div class="baloon_text"><p><b><?php echo $post->post_title;  ?></b><br/><?php echo $wpgmp_location_address_item;  ?></p></div>'}
                  <?php
                endif;
               endforeach;  endif; ?>
                ]
            };
            jQuery("#map").mapmarker({
                zoom    : 8,
                center  : '<?php  echo $custom_address; ?>',
                markers : myMarkers,
            });

        });
    </script>
    <?php } ?>


    <div class="map-google">
        <!-- <div class="map-over"></div> -->
        <div id="map" style="width: 100%; height: 800px"></div>
    </div>

    <div class="container all-back">
        <a href="<?php  echo get_permalink(2249); ?>">BACK TO ALL PROPERTIES</a>
    </div>

    <div class="container">
        <div class="house-and-land-packeage">
            <h2>House & land packages</h2>
            <p>Choose the package that best suits you</p>
            <span class="hr-box"></span>
            <?php
            $posts_array = get_field('house_&_land_packages');
            if (!empty($posts_array)): ?>
                <div class="col-md-10 slide-house">
                    <div id="carousel_post" class="carousel_post flexslider">
                        <ul class="slides">
                            <?php foreach ($posts_array as $post):
                                $post_thumbnail_id = get_post_thumbnail_id( $post->ID );
                                $img = wp_get_attachment_image( $post_thumbnail_id, array('250', '350'), "", array(  'alt' => $post->post_title) );
                                if ($img != ''):
                                    ?>
                                    <li>
                                        <div class="price-slide"><p>From</p>
                                            <?php print hydra_render_field($post->ID, 'price', 'row'); ?></div>
                                        <a title="<?php echo $post->post_title; ?>" href="<?php echo get_permalink($post->ID); ?>"><?php echo $img;   ?></a>
                                        <span>
                                            <a title="<?php echo $post->post_title; ?>" href="<?php echo get_permalink($post->ID); ?>"><?php echo $post->post_title; ?></a></span>

                                    </li>
                                    <?php
                                endif;
                            endforeach; ?>

                        </ul>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>

    <div class="box-info">
        <div class="container">
        <?php
            if( have_rows('three_boxes') ):
                $i = 1;
                while ( have_rows('three_boxes') ) : the_row(); ?>
              <div class="col-md-4">
                  <div class="row" style="background-color:<?php the_sub_field('color');?>">
                    <h3 class="title-info"><?php the_sub_field('title');?>
                        <span class="hr-box"></span>
                    </h3>
                    <div class="des-info"> <?php the_sub_field('content');?> </div>
                    <div class="content-button col-md-10"><a href="<?php the_sub_field('link');?>"><?php the_sub_field('link_text');?></a></div>
                  </div>
               </div>
              <?php  $i++; endwhile;
            endif;

            ?>
            <div class="clearfix"></div>
        </div>
    </div>

    <?php if (is_active_sidebar('slide_property')) : ?>
        <div class="slide-show-peoperty">
            <?php  
            if( ! get_field('hide_promotions',get_the_ID()) ){
                if( get_field('promotions',get_the_ID()) ){
                    dynamic_sidebar( 'slide_property' );
                    echo do_shortcode(get_field('promotions',get_the_ID()));
                }
            }
            ?>
        </div>
    <?php endif; ?>



    <?php print hydra_render_field(get_the_ID(), 'related', 'detail'); ?>
    <?php $presentation = hydra_render_group(get_the_ID(), 'presentation', 'detail'); ?>

    <?php if ($presentation): ?>
        <div class="row">
            <div class="col-md-12"><h2><?php echo __('Presentation', 'aviators'); ?></h2></div>
            <?php print $presentation; ?>
        </div>
        <hr/>
    <?php endif; ?>
</div>
<script type="text/javascript">
function fixFlexsliderHeight() {
    // Set fixed height based on the tallest slide
    var height1 = jQuery(window).height();
    var height2 = jQuery("#header-wrapper").outerHeight( true );
    //var height4 = jQuery(".carousel-box.container .flex-active-slide img").height();
    var height4 = 173;
    var height42 = height4/2;
    var result =  height1 - Math.abs(height2) - Math.abs(height42);
    console.log(height1+"///"+height2+"///"+height4+"///"+height42+"///"+result);
    jQuery(".flexslider_gallery.image.hf-property-gallery .flexslider").css({'height' : result});
}
jQuery(document).ready(function() {
    fixFlexsliderHeight();
});

jQuery(window).load(function() {
    fixFlexsliderHeight();
});

jQuery(window).resize(function() {
    fixFlexsliderHeight();
});

</script>




