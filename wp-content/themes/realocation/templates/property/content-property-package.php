<script type="text/javascript" src="<?php echo get_bloginfo('template_directory');?>/fancybox/fancybox.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_directory');?>/fancybox/fancybox.css" media="screen">
<script type="text/javascript">
    jQuery(function(){
        jQuery("a.fancybox").fancybox();
    })
</script>
<div class="property-detail">
    <div class="row">
        <div class="col-md-12">
            <h1 class="property-detail-title"><?php the_title(); ?></h1>

            <div class="property-detail-subtitle">
                <?php print hydra_render_field(get_the_ID(), 'location', 'detail'); ?>
                <?php print hydra_render_field(get_the_ID(), 'text_price', 'detail'); ?>
                <?php print hydra_render_field(get_the_ID(), 'price', 'detail'); ?>
            </div>
        </div>
        <!-- /.header-title -->
    </div>
    <!-- /.row -->

    <div class="row">
        <div class="col-md-12">
            <?php print hydra_render_field(get_the_ID(), 'gallery', 'detail'); ?>
        </div>
        <!-- /.col-md-6 -->
    </div>
    <!-- /.row -->

    <div class="row">
        <div class="col-md-4">
            <?php print hydra_render_group(get_the_ID(), 'overview', 'package'); ?>
            <?php $meta = get_post_meta( get_the_ID() ); ?>
            <?php if (isset($meta['floor_plan'][0]) && !empty($meta['floor_plan'][0])): ?>
                <div class="overview-item">
                    <h3>Floor plan</h3>
                    <a class="floor-plan fancybox" href="<?php print wp_get_attachment_image_src($meta['floor_plan'][0], 'full')[0];?>" title="<?php the_title(); ?> - Floor plan">
                        <?php print wp_get_attachment_image( $meta['floor_plan'][0], array( 262, 300) );?>
                    </a>
                </div>
            <?php endif ?>
            <?php if ((isset($meta['flyer'][0]) && !empty($meta['flyer'][0])) || (isset($meta['inclusions'][0]) && !empty($meta['inclusions'][0]))): ?>
                <div class="overview-item buttons">
                    <?php if (isset($meta['flyer'][0]) && !empty($meta['flyer'][0])): ?>
                        <a target="_blank" href="<?php print wp_get_attachment_url($meta['flyer'][0]); ?>" title="<?php the_title(); ?> - Specifications and Inclusions" class="button">Download Flyer</a>
                    <?php endif?>
                    <?php if (isset($meta['inclusions'][0]) && !empty($meta['inclusions'][0])): ?>
                        <a target="_blank" href="<?php print wp_get_attachment_url($meta['inclusions'][0]); ?>" title="<?php the_title(); ?> - Download Flyer" class="button">Specifications and Inclusions</a>
                    <?php endif?>
                </div>
            <?php endif?>
            <?php //print_r($meta);?>
        </div>
        <div class="col-md-8">
            <?php $content = get_the_content(); ?>
            <?php if (!empty($content)) : ?>
                <h2><?php echo __('Description', 'aviators'); ?></h2>
                <?php the_content(); ?>
            <?php endif; ?>
        </div>
    </div>
    <hr/>
    <?php print hydra_render_field(get_the_ID(), 'related', 'detail'); ?>
    <?php $presentation = hydra_render_group(get_the_ID(), 'presentation', 'detail'); ?>

    <?php if ($presentation): ?>
        <div class="row">
            <div class="col-md-12"><h2><?php echo __('Presentation', 'aviators'); ?></h2></div>
            <?php print $presentation; ?>
        </div>
        <hr/>
    <?php endif; ?>


    <?php $amenities = hydra_render_field(get_the_ID(), 'amenities', 'detail'); ?>
    <?php if ($amenities): ?>
        <div class="row">
            <div class="col-md-12"><h2><?php echo __('Amenities', 'aviators'); ?></h2></div>
            <div class="property-detail-amenities">
                <?php print $amenities; ?>
            </div>
        </div>
    <?php endif; ?>

</div>