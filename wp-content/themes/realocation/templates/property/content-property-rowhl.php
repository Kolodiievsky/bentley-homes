<div class="property-row">
    <div class="row">
        <div class="property-row-picture col-sm-6 col-md-6 col-lg-4">

            
               
                    <img src="<?php echo aviators_get_featured_image(get_the_ID(), 284, 184); ?>" alt="<?php the_title(); ?>">
                
                <!-- /.property-row-meta -->
           
            <!-- /.property-row-picture -->
        </div>
        <!-- /.property-row-picture -->

        <div class="property-row-content col-sm-6 col-md-6 col-lg-8 col-md-6 col-lg-8">
            <div class="property-row-data">
                <h3 class="property-row-title">
                    
                        <?php the_title(); ?>
                    
                </h3><!-- /.property-row-title -->
                <?php 
                    $address = array();
                    $address2 = array();   
                    if( get_field( 'street_number',get_the_ID() ) ){
                        $address[] = get_field( 'street_number',get_the_ID() );
                    }
                    if( get_field( 'street',get_the_ID() ) ){
                        $address[] = get_field( 'street',get_the_ID() );
                    }
                    if( get_field( 'suburb',get_the_ID() ) ){
                        $address2[] = get_field( 'suburb',get_the_ID() );
                    }
                    if( get_field( 'state',get_the_ID() ) ){
                        $address2[] = get_field( 'state',get_the_ID() );
                    }
                    if( get_field( 'country',get_the_ID() ) ){
                        $address2[] = get_field( 'country',get_the_ID() );
                    }
                    if( get_field( 'postcode',get_the_ID() ) ){
                        $address2[] = get_field( 'postcode',get_the_ID() );
                    }
                   // echo hydra_render_field(get_the_ID(), 'location', 'row'); 
                   echo "<div class='address'><span class='street'>".implode(" ", $address)."</span>"."<span class='rest'> ".implode(" ", $address2)."</span></div>";

                $key_6_value = get_post_meta( get_the_ID() , 'hf_property_land-size', true );
                 if( !empty($key_6_value)){
                     echo '<span class="size">Land Size: '.$key_6_value['items'][0]['value'].'m<sup>2</sup></span>';
                 }
                 $key_5_value = get_post_meta( get_the_ID() , 'hf_property_area', true );
                    if( !empty($key_5_value)){
                        echo '<span class="area">Area: '.$key_5_value['items'][0]['value'].'m<sup>2</sup></span>';
                    }
                ?>

                <ul class="hl_property_custom room">
                    <?php 
                    $key_1_value = get_post_meta( get_the_ID() , 'hf_property_bedrooms', true );
                    if( !empty($key_1_value)){
                        echo '<li class="beds">'.$key_1_value['items'][0]['value'].'</li>';
                    }
                    $key_2_value = get_post_meta( get_the_ID(), 'hf_property_bathrooms', true );
                    if( !empty($key_2_value)){
                        echo '<li class="baths">'.$key_2_value['items'][0]['value'].'</li>';
                    }
                    $key_3_value = get_post_meta( get_the_ID(), 'hf_property_storeys', true );
                    if( !empty($key_3_value)){
                        echo '<li class="storeys">'.$key_3_value['items'][0]['value'].'</li>';
                    }
                    $key_4_value = get_post_meta( get_the_ID() , 'hf_property_car_spaces', true );
                    if( !empty($key_4_value)){
                        echo '<li class="garages">'.$key_4_value['items'][0]['value'].'</li>';
                    }
                    ?>
                </ul>
            </div>

            <div class="property-row-price">
                <?php 
                   $key_7_value = get_post_meta( get_the_ID() , 'hf_property_text_price', true );
                    if( !empty($key_7_value)){
                        echo '<span class="from">'.$key_7_value['items'][0]['value'].'</span>';
                    }
                   echo hydra_render_field(get_the_ID(), 'price', 'row');
                   
                   echo '<a href="#" class="enquire price_but">Enquire</a>';

                    $file = get_field('flyer',get_the_ID());

                    if( $file ): ?>
                        
                        <a class="package_details price_but" href="<?php echo $file['url']; ?>" target="_blank">Flyer</a>

                    <?php endif; ?>
                      
            </div>
            <!-- /.property-row-price -->
        </div>
        <!-- /.property-row-content -->
    </div>
    <!-- /.row -->
</div>

